# OpenANM2
### An open-source ANM2 animation editor built for The Binding of Isaac: Rebirth and its DLCs.
<a href='https://ko-fi.com/opensauce' target='_blank'><img style='border:0px;height:46px;' src='https://img.shields.io/badge/Kofi-F16061.svg?logo=ko-fi&logoColor=white' border='0' alt='Support this project at ko-fi.com' />
[<img src="https://img.shields.io/badge/Trello-%23026AA7.svg?logo=Trello&logoColor=white"/>](https://trello.com/b/GDgWfll6/openanm2)
[<img src="https://tokei.rs/b1/gitlab/OpenSauce04/OpenANM2?category=code"/>]()

<table><tr>
  <td><a href=https://github.com/ruby-processing/propane><img src="https://github.com/OpenSauce04/OpenANM2/assets/48618519/4f3692e0-8104-4d79-9820-4f45c00b3fb2" alt="propane logo" width="30px"/></a></td>
  <td>Built on the <a href=https://github.com/ruby-processing/propane>Propane</a> framework</td>
</tr></table>

---

### Development Dependencies:

- JRuby >=9.4
  
  - JDK >=17

