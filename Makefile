JRUBY_URL = https://repo1.maven.org/maven2/org/jruby/jruby-complete/9.4.5.0/jruby-complete-9.4.5.0.jar
JRUBY_JAR = jruby-complete-9.4.5.0.jar

build:
# Download JRuby
	mkdir -p cache
	test -f cache/$(JRUBY_JAR) || wget -O cache/$(JRUBY_JAR) $(JRUBY_URL)

# Set up target directory
	rm -rf target
	mkdir -p target/master
	cp -r openanm2 target/master/
	cp Gemfile Gemfile.lock target/master/openanm2/
	cp cache/$(JRUBY_JAR) target/master/openanm2/bin/jruby-complete.jar

# Install gems
	cd target/master/openanm2 && \
		./jruby-workaround.sh -S bundle install

# Create platform-specific builds
	mkdir target/windows target/linux
	cp -r target/master/* target/linux/
	cp -r target/master/* target/windows/

  # Windows
	rm -rf target/windows/openanm2/bin/linux
	rm target/windows/openanm2/**.sh

  # Linux
	rm -rf target/linux/openanm2/bin/windows
	rm target/linux/openanm2/**.bat