# FAQ

## What is OpenANM2?

OpenANM2 is an animation editor for the anm2 animation format used by The Binding of Isaac: Rebirth and its DLCs.

It was created due to community dissatisfaction with the animation editor included with the game, and serves as a by-the-community for-the-community alternative.

## On what platforms can I use OpenANM2?

OpenANM2 is officially supported on Windows and Linux. 

## What can I expect?

OpenANM2 aims to act as a functional extension of the vanilla editor's featureset, taking that editor's capabilities and improving them or adding new features. Even if you have no interest in any additional features offered by this new editor, you can expect a smoother and more polished editing experience.

At present, the project is still in very active development and is not a finished product. For this reason, certain features may not be in a finalized state, but this will change over time as a 1.0 release is approached, where the project will be considered to be in a completed state (although new features may continue to be added).

## How can I contribute?

As this an open-source, community orientated project, pull requests are welcomed and encouraged. Please ensure that any code you submit is formatted in alignment with the rest of the codebase. A more fleshed out set of code requirements will be added at a later date. 

Suggestions for new features are also welcomed, and can be submitted as a GitLab issue with the "Suggestion" label.

## What does the snail icon (![snail](https://gitlab.com/OpenSauce04/OpenANM2/-/raw/main/openanm2/resources/images/slow.png?ref_type=heads)) mean?

The presence of this icon in the top right of the screen indicates that the animation editor is struggling to keep up with the animation playback and is skipping frames to keep a consistent playback speed. In most cases, this shouldn't cause much of an issue, and the effect is likely impercievable unless you're working on a very low-end system or are working on an extremely complex animation.
