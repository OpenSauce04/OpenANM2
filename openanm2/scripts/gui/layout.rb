# frozen_string_literal: true

module GuiLayoutInit
  as_trait do
    $layout = {}
    $layout[:menu_bar] = {
      height: 20
    }
    $layout[:scrollbar] = {
      thickness: 20
    }
    $layout[:animation_list] = {
      x: -200,
      y: -350,
      w: 200,
      h: 350,
      anchor_x: RIGHT,
      anchor_y: BOTTOM,
      button_bar_height: 20,
      scroll_magnitude: 10
    }
    $layout[:animation_list][:scroll_area] = {
      x: $layout[:animation_list][:x],
      y: $layout[:animation_list][:y],
      w: $layout[:animation_list][:w],
      h: $layout[:animation_list][:h],
      anchor_x: $layout[:animation_list][:anchor_x],
      anchor_y: $layout[:animation_list][:anchor_y]
    }
    $layout[:frame_editor] = {
      x: -400 + $layout[:animation_list][:x],
      y: -350,
      w: 400,
      h: 350,
      anchor_x: RIGHT,
      anchor_y: BOTTOM,
      column: [
        55,
        145,
        255,
        345,
        100,
        300
      ],
      row: [
        45,
        100,
        155,
        210,
        265,
        320,
        325
      ],
      input_offset: Vec2D.new(-10, -23),
      textfield_width: 60
    }
    $layout[:animation_panel] = {
      x: 0,
      y: -280,
      w: -$layout[:animation_list][:w] - $layout[:frame_editor][:w],
      h: 280,
      anchor_y: BOTTOM,
      anchor_w: RIGHT,
      padding: 10,
      layer_list_width: 140,
      layer_gap: 10,
      keyframe_gap: 1,
      vscroll_magnitude: 10,
      hscroll_magnitude: 15
    }
    $layout[:animation_panel][:clip] = {
        x: $layout[:animation_panel][:x],
        y: $layout[:animation_panel][:y],
        w: $layout[:animation_panel][:w] - $layout[:scrollbar][:thickness],
        h: $layout[:animation_panel][:h] - $layout[:scrollbar][:thickness],
        anchor_y: $layout[:animation_panel][:anchor_y],
        anchor_w: $layout[:animation_panel][:anchor_w]
    }

    $layout[:animation_panel][:keyframe_clip] = $layout[:animation_panel][:clip].clone
    $layout[:animation_panel][:keyframe_clip][:x] += $layout[:animation_panel][:layer_list_width]
    $layout[:animation_panel][:keyframe_clip][:w] -= $layout[:animation_panel][:layer_list_width]

    $layout[:animation_panel][:scroll_area] = $layout[:animation_panel][:clip].clone
    $layout[:animation_panel][:scroll_area][:h] -= $layout[:scrollbar][:thickness]
    $layout[:animation_panel][:scroll_area][:anchor_y] = $layout[:animation_panel][:anchor_y]
    $layout[:animation_panel][:scroll_area][:anchor_w] = $layout[:animation_panel][:anchor_w]

    $layout[:keyframe] = {
      w: 20.0,
      h: 24.0
    }

    $layout[:sprite_panel] = {
      x: -$layout[:animation_panel][:x] + $layout[:animation_panel][:w],
      y: $layout[:menu_bar][:height],
      w: -(-$layout[:animation_panel][:x] + $layout[:animation_panel][:w]),
      h: -($layout[:animation_list][:h] + $layout[:menu_bar][:height]),
      anchor_x: RIGHT,
      anchor_h: BOTTOM,
      input_offset: Vec2D.new(-10, -30),
      textfield_width: 60
    }

    $layout[:animation_seeker] = {
      x: $layout[:animation_panel][:layer_list_width],
      y: $layout[:animation_panel][:y] - 23,
      w: -$layout[:animation_list][:w] - $layout[:frame_editor][:w] - $layout[:animation_panel][:layer_list_width] - $layout[:scrollbar][:thickness],
      h: 23,
      anchor_y: $layout[:animation_panel][:anchor_y],
      anchor_w: $layout[:animation_panel][:anchor_w]
    }
    $layout[:animation_seeker][:clip] = {
      x: $layout[:animation_seeker][:x],
      y: $layout[:animation_seeker][:y],
      w: $layout[:animation_seeker][:w] - $layout[:scrollbar][:thickness],
      h: $layout[:animation_seeker][:h],
      anchor_y: $layout[:animation_seeker][:anchor_y],
      anchor_w: $layout[:animation_seeker][:anchor_w]
    }

    def gui_layout_init
      logs('Initializing GUI layout')
      init_menu_bar
      init_animation_list
      init_animation_panel
      init_frame_editor
      init_sprite_panel
      init_animation_seeker
    end
  end
end
