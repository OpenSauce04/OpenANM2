# frozen_string_literal: true

module GuiDrawableCircle
  as_trait do
    def draw_circle(x, y, r, colour)
      stroke 0, 0
      fill colour

      circle(x, y, r)
    end
  end
end
