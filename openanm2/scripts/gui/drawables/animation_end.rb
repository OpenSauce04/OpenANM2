# frozen_string_literal: true

module GuiDrawableAnimationEnd
  as_trait do
    def draw_animation_end
      clip = $layout[:animation_panel][:keyframe_clip]
      clip(
        clip[:x],
        clip[:y] + anchor_position(clip[:anchor_y]),
        clip[:w] + anchor_position(clip[:anchor_w]),
        clip[:h]
      )
      x = $layout[:animation_panel][:padding] +
          $layout[:animation_panel][:layer_list_width] +
          $anm_data[:duration]*$layout[:keyframe][:w] -
          get_hscrollbar_position(:animation_panel_hscroll)
      draw_rect(x, 0, width, height, color(0, 100))
      no_clip
    end
  end
end
