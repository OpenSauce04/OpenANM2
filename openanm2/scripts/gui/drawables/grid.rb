# frozen_string_literal: true

module GuiDrawableGrid
  as_trait do
    def draw_grid
      step = ($viewer_info[:zoom]*100) / (2**Math.log2($viewer_info[:zoom]).round) #? Can this be simplified?
      (0..width/step).each do |i|
        xoffset = $viewer_info[:view_offset].x%step
        yoffset = $viewer_info[:view_offset].y%step
        draw_line(i*step + xoffset, 0, i*step + xoffset, height, $theme[:background_light])
        draw_line(0, i*step + yoffset, width, i*step + yoffset, $theme[:background_light])
      end

      # Center lines
      draw_line($viewer_info[:view_offset].x, 0, $viewer_info[:view_offset].x, height, $theme[:foreground])
      draw_line(0, $viewer_info[:view_offset].y, width, $viewer_info[:view_offset].y, $theme[:foreground])
    end
  end
end
