# frozen_string_literal: true

module GuiDrawableImage
  as_trait do
    def draw_image(image, x, y, w, h)
      tint(255, 255)
      image image, x, y, w, h
    end
  end
end
