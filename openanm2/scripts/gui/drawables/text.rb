# frozen_string_literal: true

module GuiDrawableText
  as_trait do
    def draw_text(text, x, y, align_x = LEFT, align_y = TOP, is_symbol = false)
      stroke $theme[:text]
      fill $theme[:text]
      text_align align_x, align_y
      if is_symbol
        text_font($fonts[:symbol])
      else
        text_font($fonts[:regular])
      end
      text(text, x, y)
    end
  end
end
