# frozen_string_literal: true

module GuiDrawableLine
  as_trait do
    def draw_line(x1, y1, x2, y2, colour)
      fill 0, 0
      stroke colour

      # Draw the box
      line x1, y1, x2, y2
    end
  end
end
