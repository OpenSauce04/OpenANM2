# frozen_string_literal: true

module GuiDrawableQuad
  as_trait do
    def draw_quad(x1, y1, x2, y2, x3, y3, x4, y4, colour)
      stroke 0, 0
      fill colour

      quad(x1, y1, x2, y2, x3, y3, x4, y4)
    end
  end
end
