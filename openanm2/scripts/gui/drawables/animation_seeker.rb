# frozen_string_literal: true

module GuiDrawableAnimationSeeker
  as_trait do
    def draw_animation_seeker
      clip = $layout[:animation_panel][:keyframe_clip]
      clip(
        clip[:x],
        clip[:y] + anchor_position(clip[:anchor_y]),
        clip[:w] + anchor_position(clip[:anchor_w]),
        clip[:h]
      )
      x = $layout[:animation_panel][:padding] +
          $layout[:animation_panel][:layer_list_width] +
          $layout[:keyframe][:w]/2 +
          $viewer_info[:animation_timer]*$layout[:keyframe][:w] -
          get_hscrollbar_position(:animation_panel_hscroll)
      draw_line(x, 0, x, height, color(255, 120))
      no_clip
    end
  end
end
