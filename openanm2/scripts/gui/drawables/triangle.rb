# frozen_string_literal: true

module GuiDrawableTriangle
  as_trait do
    def draw_triangle(x1, y1, x2, y2, x3, y3, colour)
      stroke 0, 0
      fill colour

      triangle(x1, y1, x2, y2, x3, y3)
    end
  end
end
