# frozen_string_literal: true

module GuiDrawableRect
  as_trait do
    def draw_rect(x, y, w, h, colour, r = 0)
      stroke 0, 0
      fill colour

      rect(x, y, w, h, r)
    end
  end
end
