# frozen_string_literal: true

module ViewerFunctions
  as_trait do
    def pan_viewer
      $program_info[:cursor] = MOVE
      $viewer_info[:view_offset].x += (mouse_x - pmouse_x)
      $viewer_info[:view_offset].y += (mouse_y - pmouse_y)
    end

    def zoom_viewer(amount)
      $viewer_info[:zoom] += ($viewer_info[:zoom]/10)*amount.to_f
    end

    def toggle_animation_paused
      if $viewer_info[:paused]
        $viewer_info[:animation_timer_start] = millis - $viewer_info[:animation_timer]*(1000/$anm_data[:framerate])
        $viewer_info[:paused] = false
      else
        $viewer_info[:paused] = true
      end
    end
  end
end
