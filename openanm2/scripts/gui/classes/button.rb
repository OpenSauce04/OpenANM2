# frozen_string_literal: true

module GuiClassButton
  class Button < GuiObject
    attr_reader :group, :text, :is_symbol, :function, :colour_override, :attached_vscrollbar, :clip, :anchor_x, :anchor_y

    def initialize(attr)
      @group = attr[:group]
      @text = attr[:text]
      @is_symbol = attr[:is_symbol]
      @x = attr[:x]
      @y = attr[:y]
      @w = attr[:w]
      @h = attr[:h]
      @function = attr[:function]
      @anchor_x = attr[:anchor_x]
      @anchor_y = attr[:anchor_y]
      @colour_override = attr[:colour_override]
      @attached_vscrollbar = attr[:attached_vscrollbar]
      @clip = attr[:clip]
    end
  end

  as_trait do
    def add_button(attr)
      $gui_objs[:buttons].append Button.new(attr)
    end

    def mouse_in_button(b)
      scrolled_y = b.y - get_vscrollbar_position(b.attached_vscrollbar)
      in_clip_box = if b.clip.nil?
                      true
                    else
                      mouse_in_box(
                        b.clip[:x] + anchor_position(b.anchor_x),
                        b.clip[:y] + anchor_position(b.anchor_y),
                        b.clip[:w],
                        b.clip[:h]
                      )
                    end
      return mouse_in_box(b.x, scrolled_y, b.w, b.h) && in_clip_box
    end

    def render_buttons
      $gui_objs[:buttons].each do |b|
        unless b.clip.nil?
          clip(
            b.clip[:x] + anchor_position(b.anchor_x),
            b.clip[:y] + anchor_position(b.anchor_y),
            b.clip[:w],
            b.clip[:h]
          )
        end
        scrolled_y = b.y - get_vscrollbar_position(b.attached_vscrollbar)
        $program_info[:cursor] = HAND if mouse_in_button(b)
        colour = if mouse_in_button(b) && mouse_pressed? && mouse_button == LEFT
                   $theme[:foreground_dark]
                 elsif b.colour_override
                   b.colour_override
                 else
                   $theme[:foreground]
                 end

        draw_rect b.x, scrolled_y, b.w, b.h, colour
        draw_text b.text, b.x + b.w/2, scrolled_y + b.h/2 - 2, CENTER, CENTER, b.is_symbol
        no_clip
      end
    end

    def click_buttons
      $gui_objs[:buttons].each do |b|
        if mouse_in_button(b) && $program_info[:last_mouse_button] == LEFT
          b.function.call
          break
        end
      end
    end
  end
end
