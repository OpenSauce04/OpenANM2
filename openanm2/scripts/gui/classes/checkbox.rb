# frozen_string_literal: true

module GuiClassCheckbox
  class Checkbox < GuiObject
    attr_reader :group, :function, :colour_override, :state, :attached_vscrollbar, :clip, :anchor_x, :anchor_y

    def initialize(attr)
      @group = attr[:group]
      @x = attr[:x]
      @y = attr[:y]
      @w = 20
      @h = 20
      @function = attr[:function]
      @state = attr[:state]
      @anchor_x = attr[:anchor_x]
      @anchor_y = attr[:anchor_y]
      @colour_override = attr[:colour_override]
      @attached_vscrollbar = attr[:attached_vscrollbar]
      @clip = attr[:clip]
    end
  end

  as_trait do
    def add_checkbox(attr)
      $gui_objs[:checkboxes].append Checkbox.new(attr)
    end

    def mouse_in_checkbox(c)
      in_clip_box = if c.clip.nil?
                      true
                    else
                      mouse_in_box(
                        c.clip[:x] + anchor_position(c.anchor_x),
                        c.clip[:y] + anchor_position(c.anchor_y),
                        c.clip[:w],
                        c.clip[:h]
                      )
                    end
      scrolled_y = c.y - get_vscrollbar_position(c.attached_vscrollbar)

      return mouse_in_box(c.x, scrolled_y, c.w, c.h) && in_clip_box
    end

    def render_checkboxes
      $gui_objs[:checkboxes].each do |c|
        unless c.clip.nil?
          clip(
            c.clip[:x] + anchor_position(c.anchor_x),
            c.clip[:y] + anchor_position(c.anchor_y),
            c.clip[:w],
            c.clip[:h]
          )
        end
        scrolled_y = c.y - get_vscrollbar_position(c.attached_vscrollbar)
        $program_info[:cursor] = HAND if mouse_in_checkbox(c)
        colour = if mouse_in_checkbox(c) && mouse_pressed? && mouse_button == LEFT
                   $theme[:foreground_dark]
                 elsif c.state == true
                   $theme[:accent]
                 else
                   $theme[:foreground]
                 end

        draw_rect c.x, scrolled_y, c.w, c.h, colour, 5
        no_clip
      end
    end

    def click_checkboxes
      $gui_objs[:checkboxes].each do |c|
        scrolled_y = c.y - get_vscrollbar_position(c.attached_vscrollbar)
        in_clip_box = if c.clip.nil?
                        true
                      else
                        mouse_in_box(
                          c.clip[:x] + anchor_position(c.anchor_x),
                          c.clip[:y] + anchor_position(c.anchor_y),
                          c.clip[:w],
                          c.clip[:h]
                        )
                      end
        if mouse_in_box(c.x, scrolled_y, c.w, c.h) && in_clip_box && $program_info[:last_mouse_button] == LEFT
          c.function.call
          break
        end
      end
    end
  end
end
