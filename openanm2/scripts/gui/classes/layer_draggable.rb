# frozen_string_literal: true

module GuiClassLayerDraggable
  class LayerDraggable < GuiObject
    attr_reader :group, :layeranim, :anchor_x, :anchor_y, :clip, :attached_vscrollbar

    def initialize(attr)
      @group = :animation_panel
      @layeranim = attr[:layeranim]
      @x = attr[:x]
      @y = attr[:y]
      @w = attr[:w]
      @h = attr[:h]
      @anchor_x = $layout[:animation_panel][:anchor_x]
      @anchor_y = $layout[:animation_panel][:anchor_y]
      @clip = attr[:clip]
      @attached_vscrollbar = attr[:attached_vscrollbar]
    end
  end

  as_trait do
    def add_layer_draggable(attr)
      $gui_objs[:layer_draggables].append LayerDraggable.new(attr)
    end

    def mouse_in_layer_draggable(h)
      scrolled_y = h.y - get_vscrollbar_position(h.attached_vscrollbar)
      in_clip_box = if h.clip.nil?
                      true
                    else
                      mouse_in_box(
                        h.clip[:x] + anchor_position(h[:anchor_x]),
                        h.clip[:y] + anchor_position(h[:anchor_y]),
                        h.clip[:w],
                        h.clip[:h]
                      )
                    end
      return mouse_in_box(h.x, scrolled_y, h.w, h.h) && in_clip_box
    end

    def begin_drag_layers
      $gui_objs[:layer_draggables].each do |h|
        if mouse_in_layer_draggable(h)
          $program_info[:dragging_layer] = h
          break
        end
      end
    end

    def drag_layers
      return if $program_info[:dragging_layer] == nil

      $gui_objs[:layer_draggables].each do |h|
        if mouse_in_layer_draggable(h) && h.layeranim.get_string('LayerId') != $program_info[:dragging_layer].layeranim.get_string('LayerId')
          swap_layeranims($program_info[:dragging_layer].layeranim, h.layeranim)
          load_animation($anm_data[:current_animation_name])
          break
        end
      end
      $program_info[:dragging_layer] = nil
    end
  end
end
