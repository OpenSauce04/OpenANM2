# frozen_string_literal: true

class GuiObject
  def anchor_position(anchor)
    pos = 0
    pos = $screen_size.x if anchor == RIGHT
    pos = $screen_size.y if anchor == BOTTOM

    return pos
  end

  def x
    return @x + anchor_position(@anchor_x)
  end

  def y
    return @y + anchor_position(@anchor_y)
  end

  def w
    return @w + anchor_position(@anchor_w)
  end

  def h
    return @h + anchor_position(@anchor_h)
  end

  def x1
    return @x1 + anchor_position(@anchor_x1)
  end

  def y1
    return @y1 + anchor_position(@anchor_y1)
  end

  def x2
    return @x2 + anchor_position(@anchor_x2)
  end

  def y2
    return @y2 + anchor_position(@anchor_y2)
  end
end
