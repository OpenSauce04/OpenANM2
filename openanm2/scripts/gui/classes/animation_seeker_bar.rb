# frozen_string_literal: true

module AnimationSeekerBar
  def click_animation_seeker_bar
    frameno = (mouse_x + get_hscrollbar_position(:animation_panel_hscroll)-($layout[:animation_panel][:padding] +
              $layout[:animation_panel][:layer_list_width] +
              $layout[:keyframe][:w]/2)) / $layout[:keyframe][:w]
    if mouse_pressed_in_box(
    $layout[:animation_seeker][:x],
    $layout[:animation_seeker][:y] + anchor_position($layout[:animation_seeker][:anchor_y]),
    $layout[:animation_seeker][:w] + anchor_position($layout[:animation_seeker][:anchor_w]),
    $layout[:animation_seeker][:h])
      $program_info[:cursor] = MOVE
      $viewer_info[:animation_timer] = frameno.clamp(0, $anm_data[:duration]-1).round
      $viewer_info[:paused] = true
    elsif mouse_in_box(
    $layout[:animation_seeker][:x],
    $layout[:animation_seeker][:y] + anchor_position($layout[:animation_seeker][:anchor_y]),
    $layout[:animation_seeker][:w] + anchor_position($layout[:animation_seeker][:anchor_w]),
    $layout[:animation_seeker][:h])
      $program_info[:cursor] = HAND
    end
  end
end
