# frozen_string_literal: true

module GuiClassLabel
  class Label < GuiObject
    attr_reader :group, :clip, :name, :text, :is_symbol, :align_x, :align_y, :anchor_x, :anchor_y, :attached_vscrollbar, :attached_hscrollbar

    def initialize(attr)
      @group = attr[:group]
      @clip = attr[:clip]
      @text = attr[:text]
      @is_symbol = attr[:is_symbol]
      @x = attr[:x]
      @y = attr[:y]
      @align_x = attr[:align_x]
      @align_y = attr[:align_y]
      @anchor_x = attr[:anchor_x]
      @anchor_y = attr[:anchor_y]
      @attached_vscrollbar = attr[:attached_vscrollbar]
      @attached_hscrollbar = attr[:attached_hscrollbar]
    end
  end

  as_trait do
    def add_label(attr)
      $gui_objs[:labels].append Label.new(attr)
    end

    def render_labels
      $gui_objs[:labels].each do |l|
        unless l.clip.nil?
          clip(
            l.clip[:x] + anchor_position(l.anchor_x),
            l.clip[:y] + anchor_position(l.anchor_y),
            l.clip[:w] + anchor_position(l.clip[:anchor_w]),
            l.clip[:h] + anchor_position(l.clip[:anchor_h])
          )
        end
        scrolled_x = l.x - get_hscrollbar_position(l.attached_hscrollbar)
        scrolled_y = l.y - get_vscrollbar_position(l.attached_vscrollbar)
        draw_text l.text, scrolled_x, scrolled_y, l.align_x, l.align_y, l.is_symbol
        no_clip
      end
    end
  end
end
