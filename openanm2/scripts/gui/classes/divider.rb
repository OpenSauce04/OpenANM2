# frozen_string_literal: true

module GuiClassDivider
  class Divider < GuiObject
    attr_reader :group, :clip, :colour, :attached_vscrollbar, :attached_hscrollbar

    def initialize(attr)
      @group = attr[:group]
      @x1 = attr[:x1]
      @y1 = attr[:y1]
      @x2 = attr[:x2]
      @y2 = attr[:y2]
      @anchor_x1 = attr[:anchor_x1]
      @anchor_y1 = attr[:anchor_y1]
      @anchor_x2 = attr[:anchor_x2]
      @anchor_y2 = attr[:anchor_y2]
      @clip = attr[:clip]
      @colour = attr[:colour]
      @attached_vscrollbar = attr[:attached_vscrollbar]
      @attached_hscrollbar = attr[:attached_hscrollbar]
    end
  end

  as_trait do
    def add_divider(attr)
      $gui_objs[:dividers].append Divider.new(attr)
    end

    def render_dividers
      $gui_objs[:dividers].each do |d|
        unless d.clip.nil?
          clip(
            d.clip[:x] + anchor_position(d.clip[:anchor_x]),
            d.clip[:y] + anchor_position(d.clip[:anchor_y]),
            d.clip[:w] + anchor_position(d.clip[:anchor_w]),
            d.clip[:h] + anchor_position(d.clip[:anchor_h])
          )
        end
        scrolled_x1 = d.x1 - get_hscrollbar_position(d.attached_hscrollbar)
        scrolled_x2 = d.x2 - get_hscrollbar_position(d.attached_hscrollbar)
        scrolled_y1 = d.y1 - get_vscrollbar_position(d.attached_vscrollbar)
        scrolled_y2 = d.y2 - get_vscrollbar_position(d.attached_vscrollbar)
        draw_line scrolled_x1, scrolled_y1, scrolled_x2, scrolled_y2, d.colour
        no_clip
      end
    end
  end
end
