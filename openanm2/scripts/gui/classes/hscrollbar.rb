# frozen_string_literal: true

module GuiClassHScrollbar
  class HScrollbar < GuiObject
    attr_reader :group, :name, :anchor_x, :anchor_y, :anchor_w, :scroll_maximum, :scroll_magnitude
    attr_accessor :scroll_position

    def initialize(attr)
      @group = attr[:group]
      @name = attr[:name]
      @x = attr[:x]
      @y = attr[:y]
      @w = attr[:w]
      @h = $layout[:scrollbar][:thickness]
      @anchor_x = attr[:anchor_x]
      @anchor_y = attr[:anchor_y]
      @anchor_w = attr[:anchor_w]
      @scroll_position = attr[:scroll_position]
      @scroll_maximum = attr[:scroll_maximum]
      @scroll_magnitude = attr[:scroll_magnitude]
    end
  end

  as_trait do
    def add_hscrollbar(attr)
      $gui_objs[:hscrollbars].append HScrollbar.new(attr)
    end

    def render_hscrollbars
      $gui_objs[:hscrollbars].each do |s|
        draw_rect s.x, s.y, s.w, s.h, $theme[:foreground_dark]
        draw_rect s.x+(s.scroll_position/s.scroll_maximum * s.w) - 1, s.y, 2, s.h, $theme[:foreground]
      end
    end

    def click_hscrollbars
      return unless mouse_pressed? && mouse_button == LEFT
      $gui_objs[:hscrollbars].each do |s|
        if mouse_in_box(s.x, s.y, s.w, s.h)
          s.scroll_position = ((mouse_x - s.x - 1).to_f / s.w) * s.scroll_maximum
          break
        end
      end
    end

    def scroll_hscrollbars(amount)
      $gui_objs[:hscrollbars].each do |s|
        if mouse_in_box(s.x, s.y, s.w, s.h)
          s.scroll_magnitude.times do
            s.scroll_position = (s.scroll_position+amount).clamp(0, s.scroll_maximum).to_f
          end
          break
        end
      end
    end

    def get_hscrollbar_position(name)
      return 0 if name.nil? || $gui_objs[:hscrollbars].empty?
      $gui_objs[:hscrollbars].each do |s|
        return s.scroll_position.round if s.name == name
      end
    end
  end
end
