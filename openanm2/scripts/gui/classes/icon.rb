# frozen_string_literal: true

module GuiClassIcon
  class Icon < GuiObject
    attr_reader :group, :image, :anchor_x, :anchor_y

    def initialize(attr)
      @group = attr[:group]
      @image = attr[:image]
      @x = attr[:x]
      @y = attr[:y]
      @w = attr[:w]
      @h = attr[:h]
      @anchor_x = attr[:anchor_x]
      @anchor_y = attr[:anchor_y]
    end
  end

  as_trait do
    def add_icon(attr)
      $gui_objs[:icons].append Icon.new(attr)
    end

    def render_icons
      $gui_objs[:icons].each do |i|
        draw_image i.image, i.x, i.y, i.w, i.h
      end
    end
  end
end
