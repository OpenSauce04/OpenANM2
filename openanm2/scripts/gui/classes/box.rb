# frozen_string_literal: true

module GuiClassBox
  class Box < GuiObject
    attr_reader :group, :name, :colour, :anchor_x, :anchor_y

    def initialize(attr)
      @group = attr[:group]
      @name = attr[:name]
      @x = attr[:x]
      @y = attr[:y]
      @w = attr[:w]
      @h = attr[:h]
      @colour = attr[:colour]
      @anchor_x = attr[:anchor_x]
      @anchor_y = attr[:anchor_y]
      @anchor_w = attr[:anchor_w]
      @anchor_h = attr[:anchor_h]
    end
  end

  as_trait do
    def add_box(attr)
      $gui_objs[:boxes].append Box.new(attr)
    end

    def render_boxes
      $gui_objs[:boxes].each do |b|
        draw_rect b.x, b.y, b.w, b.h, b.colour
      end
    end
  end
end
