# frozen_string_literal: true

module GuiClassAnmLayer
  class AnmLayer
    attr_reader :group, :sprite, :frames

    def initialize(attr)
      @group = attr[:group]
      @sprite = attr[:sprite]
      @frames = attr[:frames]
    end
  end

  as_trait do
    def add_anmlayer(attr)
      $gui_objs[:anmlayers].append AnmLayer.new(attr)
    end

    def render_anmlayers(pgoverride = nil)
      return if $gui_objs[:anmlayers].length == 0

      pg = if pgoverride.nil?
             g
           else
             pgoverride
           end

      root_frame = if $viewer_info[:animation_timer] >= $anm_data[:baked_root_animation].length
                     $anm_data[:baked_root_animation][$anm_data[:baked_root_animation].length-1]
                   else
                     $anm_data[:baked_root_animation][$viewer_info[:animation_timer]]
                   end

      root_visible = root_frame.get_string('Visible').to_bool
      return if not root_visible

      root_xposition = root_frame.get_string('XPosition').to_f
      root_yposition = root_frame.get_string('YPosition').to_f
      root_xscale = root_frame.get_string('XScale').to_f
      root_yscale = root_frame.get_string('YScale').to_f
      root_rotation = root_frame.get_string('Rotation').to_f
      root_red_tint = root_frame.get_string('RedTint').to_f
      root_green_tint = root_frame.get_string('GreenTint').to_f
      root_blue_tint = root_frame.get_string('BlueTint').to_f
      root_alpha_tint = root_frame.get_string('AlphaTint').to_f
      root_red_offset = root_frame.get_string('RedOffset').to_f
      root_green_offset = root_frame.get_string('GreenOffset').to_f
      root_blue_offset = root_frame.get_string('BlueOffset').to_f

      $gui_objs[:anmlayers].each do |layer|
        next if layer.frames.length == 0

        frame = if $viewer_info[:animation_timer] >= layer.frames.length
                  layer.frames[layer.frames.length-1]
                else
                  layer.frames[$viewer_info[:animation_timer]]
                end

        visible = frame.get_string('Visible').to_bool
        next if (not visible) || layer.sprite.nil?
        xcrop = frame.get_string('XCrop').to_f
        ycrop = frame.get_string('YCrop').to_f
        fwidth = frame.get_string('Width').to_f
        fheight = frame.get_string('Height').to_f
        xposition = frame.get_string('XPosition').to_f
        yposition = frame.get_string('YPosition').to_f
        xpivot = frame.get_string('XPivot').to_f
        ypivot = frame.get_string('YPivot').to_f
        xscale = frame.get_string('XScale').to_f
        yscale = frame.get_string('YScale').to_f
        rotation = frame.get_string('Rotation').to_f
        red_tint = frame.get_string('RedTint').to_f
        green_tint = frame.get_string('GreenTint').to_f
        blue_tint = frame.get_string('BlueTint').to_f
        alpha_tint = frame.get_string('AlphaTint').to_f
        red_offset = frame.get_string('RedOffset').to_f
        green_offset = frame.get_string('GreenOffset').to_f
        blue_offset = frame.get_string('BlueOffset').to_f

        pg.push_matrix

        if pgoverride.nil?
          pg.translate $viewer_info[:view_offset].x, $viewer_info[:view_offset].y
          pg.scale $viewer_info[:zoom], $viewer_info[:zoom]
        else
          pg.translate pg.width/2, pg.height/2
          pg.scale 2, 2
        end

        # TODO: Check if this is is accurate to how the base game handles root tints
        pg.tint(red_tint * (root_red_tint/255),
                green_tint * (root_green_tint/255),
                blue_tint * (root_blue_tint/255),
                alpha_tint * (root_alpha_tint/255))

        layer_sprite = layer.sprite.get(
          xcrop,
          ycrop,
          fwidth,
          fheight
        )
        if red_offset != 0 || # If there is no offset values set, don't bother with the offset calc.
         green_offset != 0 ||
         blue_offset != 0 ||
         root_red_offset != 0 ||
         root_green_offset != 0 ||
         root_blue_offset != 0
          if pgoverride.nil? # If we're writing to PGraphics (non-P2D), we must implement colour offset without shaders. This is slower, but is required, as P2D (the renderer that can use shaders) does not support transparency.
            colour_offset = $shaders[:colour_offset]  #
            colour_offset.set('ro', red_offset/255 + root_red_offset/255)
            colour_offset.set('go', green_offset/255 + root_green_offset/255)
            colour_offset.set('bo', blue_offset/255 + root_blue_offset/255)
            pg.shader $shaders[:colour_offset]
          else
            layer_sprite.load_pixels
            (0..layer_sprite.width*layer_sprite.height - 1).each do |loc|
              p = layer_sprite.pixels[loc]
              layer_sprite.pixels[loc] = color(
                red(p) + red_offset + root_red_offset,
                green(p) + green_offset + root_green_offset,
                blue(p) + blue_offset + root_blue_offset,
                alpha(p)
              )
            end
            layer_sprite.update_pixels
          end
        end
        pg.rotate root_rotation.radians
        pg.scale root_xscale/100, root_yscale/100
        pg.translate (xposition + root_xposition), (yposition + root_yposition)
        pg.rotate rotation.radians
        pg.scale xscale/100, yscale/100

        pg.image(
          layer_sprite,
          -xpivot,
          -ypivot
        )

        pg.reset_shader
        pg.pop_matrix
      end
    end
  end
end
