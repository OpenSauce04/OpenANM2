# frozen_string_literal: true

module GuiClassVScrollbar
  class VScrollbar < GuiObject
    attr_reader :group, :name, :anchor_x, :anchor_y, :scroll_maximum, :scroll_magnitude, :scroll_area
    attr_accessor :scroll_position

    def initialize(attr)
      @group = attr[:group]
      @name = attr[:name]
      @x = attr[:x]
      @y = attr[:y]
      @w = $layout[:scrollbar][:thickness]
      @h = attr[:h]
      @anchor_x = attr[:anchor_x]
      @anchor_y = attr[:anchor_y]
      @scroll_position = attr[:scroll_position]
      @scroll_maximum = attr[:scroll_maximum]
      @scroll_magnitude = attr[:scroll_magnitude]
      @scroll_area = attr[:scroll_area]
    end
  end

  as_trait do
    def add_vscrollbar(attr)
      $gui_objs[:vscrollbars].append VScrollbar.new(attr)
    end

    def render_vscrollbars
      $gui_objs[:vscrollbars].each do |s|
        draw_rect s.x, s.y, s.w, s.h, $theme[:foreground_dark]
        draw_rect s.x, s.y+(s.scroll_position/s.scroll_maximum * s.h) - 1, s.w, 2, $theme[:foreground]
      end
    end

    def click_vscrollbars
      return unless mouse_pressed? && mouse_button == LEFT
      $gui_objs[:vscrollbars].each do |s|
        if mouse_in_box(s.x, s.y, s.w, s.h)
          s.scroll_position = ((mouse_y - s.y - 1).to_f / s.h) * s.scroll_maximum
          break
        end
      end
    end

    def scroll_vscrollbars(amount)
      $gui_objs[:vscrollbars].each do |s|
        in_scroll_area = !s.scroll_area.nil? && mouse_in_box(
          s.scroll_area[:x] + anchor_position(s.scroll_area[:anchor_x]),
          s.scroll_area[:y] + anchor_position(s.scroll_area[:anchor_y]),
          s.scroll_area[:w] + anchor_position(s.scroll_area[:anchor_w]),
          s.scroll_area[:h] + anchor_position(s.scroll_area[:anchor_h])
        )
        if mouse_in_box(s.x, s.y, s.w, s.h) || in_scroll_area
          s.scroll_magnitude.times do
            s.scroll_position = (s.scroll_position+amount).clamp(0, s.scroll_maximum).to_f
          end
          break
        end
      end
    end

    def get_vscrollbar_position(name)
      return 0 if name.nil? || $gui_objs[:vscrollbars].empty?
      $gui_objs[:vscrollbars].each do |s|
        return s.scroll_position.round if s.name == name
      end
      return 0
    end
  end
end
