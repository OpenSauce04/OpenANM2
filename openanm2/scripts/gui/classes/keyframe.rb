# frozen_string_literal: true

module GuiClassKeyframe
  class Keyframe < GuiObject
    attr_reader :group, :clip, :interpolated, :visible, :colour_override, :anchor_y, :attached_vscrollbar, :attached_hscrollbar, :frame, :is_root, :frame_position, :id, :frame_id, :layerno

    def initialize(attr)
      @group = :animation_panel
      @id = attr[:id]
      @frame_id = attr[:frame_id]
      @layerno = attr[:layerno]
      @frame = attr[:frame]
      @interpolated = @frame.get_string('Interpolated').to_bool
      @visible = @frame.get_string('Visible').to_bool
      @x = attr[:x] + $layout[:animation_panel][:padding] + $layout[:animation_panel][:layer_list_width]
      @y = $layout[:animation_panel][:y] +
           $layout[:animation_panel][:layer_gap]/2 +
           ($layout[:keyframe][:h]+$layout[:animation_panel][:layer_gap])*(@layerno)
      @w = $layout[:keyframe][:w] * @frame.get_string('Delay').to_i - $layout[:animation_panel][:keyframe_gap]
      @h = $layout[:keyframe][:h]
      @colour_override = attr[:colour_override]
      @anchor_y = $layout[:animation_panel][:anchor_y]
      @clip = $layout[:animation_panel][:keyframe_clip]
      @attached_vscrollbar = attr[:attached_vscrollbar]
      @attached_hscrollbar = attr[:attached_hscrollbar]
      @is_root = attr[:is_root]
      @frame_position = attr[:frame_position]
    end
  end

  as_trait do
    def add_keyframe(attr)
      $gui_objs[:keyframes].append Keyframe.new(attr)
    end

    def mouse_in_keyframe(k)
      scrolled_x = k.x - get_hscrollbar_position(k.attached_hscrollbar)
      scrolled_y = k.y - get_vscrollbar_position(k.attached_vscrollbar)
      in_clip_box = if k.clip.nil?
                      true
                    else
                      mouse_in_box(
                        k.clip[:x],
                        k.clip[:y] + anchor_position($layout[:animation_panel][:anchor_y]),
                        k.clip[:w] + anchor_position($layout[:animation_panel][:anchor_w]),
                        k.clip[:h]
                      )
                    end
      return mouse_in_box(scrolled_x, scrolled_y, k.w, k.h) && in_clip_box
    end

    def render_keyframes
      $gui_objs[:keyframes].each do |k|
        scrolled_x = k.x - get_hscrollbar_position(k.attached_hscrollbar)
        scrolled_y = k.y - get_vscrollbar_position(k.attached_vscrollbar)

        next if !box_in_box(
          scrolled_x,
          scrolled_y,
          k.w,
          k.h,
          k.clip[:x],
          k.clip[:y] + anchor_position($layout[:animation_panel][:anchor_y]),
          k.clip[:w] + anchor_position($layout[:animation_panel][:anchor_w]),
          k.clip[:h]
        )

        unless k.clip.nil?
          clip(
            k.clip[:x],
            k.clip[:y] + anchor_position($layout[:animation_panel][:anchor_y]),
            k.clip[:w] + anchor_position($layout[:animation_panel][:anchor_w]),
            k.clip[:h]
          )
        end

        $program_info[:cursor] = CROSS if mouse_in_keyframe(k)
        colour = if (not $program_info[:selected_keyframe].nil?) && $program_info[:selected_keyframe].id == k.id
                   $theme[:foreground]
                 elsif k.colour_override
                   k.colour_override
                 elsif k.is_root
                   $theme[:root_layer]
                 else
                   $theme[:accent]
                 end
        colour = color(red(colour), green(colour), blue(colour), 127) if not k.visible
        draw_rect(scrolled_x, scrolled_y, k.w, k.h, colour, 5)
        if k.interpolated
          draw_quad(
            scrolled_x, scrolled_y+k.h/2,
            scrolled_x+k.w/2, scrolled_y+k.h*(1.0/4.0),
            scrolled_x+k.w, scrolled_y+k.h/2,
            scrolled_x+k.w/2, scrolled_y+k.h*(3.0/4.0),
            color(0, 60)
          )
          draw_quad(
            scrolled_x, scrolled_y+k.h/2,
            scrolled_x+k.w/2, scrolled_y+k.h*(3.0/4.0),
            scrolled_x+k.w/2, scrolled_y+k.h,
            scrolled_x, scrolled_y+k.h,
            color(0, 120)
          )
          draw_quad(
            scrolled_x+k.w, scrolled_y+k.h/2,
            scrolled_x+k.w/2, scrolled_y+k.h*(3.0/4.0),
            scrolled_x+k.w/2, scrolled_y+k.h,
            scrolled_x+k.w, scrolled_y+k.h,
            color(0, 120)
          )
        else
          draw_rect(scrolled_x, scrolled_y+k.h/2, k.w, k.h/2, color(0, 120))
        end
        if (mouse_in_keyframe(k) && mouse_pressed? && mouse_button == LEFT) ||
        $program_info[:dragging_keyframe].object_id == k.object_id
          draw_rect(scrolled_x, scrolled_y, k.w, k.h, color(0, 100))
        end
        draw_circle(scrolled_x-0.5+$layout[:keyframe][:w]/2.0, scrolled_y+k.h/2.0, 8, $theme[:foreground_darkest])
        no_clip
      end
      no_clip
    end

    def click_keyframes
      $gui_objs[:keyframes].each do |k|
        if mouse_in_keyframe(k) && $program_info[:last_mouse_button] == LEFT
          $program_info[:selected_keyframe] = k
          generate_frame_editor_content(k.frame, k.is_root)
          $viewer_info[:animation_timer] = k.frame_position
          break
        end
      end
    end

    def delete_keyframe(keyframe)
      return if keyframe.nil?

      root_length = $anm_data[:root_animation].get_children('Frame').length

      $gui_objs[:keyframes].each do |k|
        if k.id == keyframe.id &&
         !(k.frame.get_parent.equal?($anm_data[:current_animation].get_child('RootAnimation')) && root_length == 1)
          k.frame.get_parent.remove_child(k.frame)
          $program_info[:selected_keyframe] = nil
          load_animation($anm_data[:current_animation_name])
          break
        end
      end
    end

    def insert_keyframe(keyframe, destination_keyframe, insert_after = false)
      return if keyframe.nil? ||
                destination_keyframe.nil?

      tmp_keyframes = []
      parent = destination_keyframe.frame.get_parent

      # Move all keyframes after the selected one into a temporary array
      $gui_objs[:keyframes].each do |k|
        if (k.frame_id > destination_keyframe.frame_id && insert_after) ||
         (k.frame_id >= destination_keyframe.frame_id && !insert_after) &&
         k.layerno == destination_keyframe.layerno
          tmp_keyframes.append(k.frame)
          parent.remove_child(k.frame)
        end
      end

      parent.add_child(keyframe.frame)

      # Add back other frames
      tmp_keyframes.each do |k|
        parent.add_child(k)
      end

      # Reload
      load_animation($anm_data[:current_animation_name])
    end

    def begin_drag_keyframes
      $gui_objs[:keyframes].each do |k|
        if mouse_in_keyframe(k)
          $program_info[:dragging_keyframe] = k
          break
        end
      end
    end

    def drag_keyframes
      return if $program_info[:dragging_keyframe] == nil

      $gui_objs[:keyframes].each do |k|
        if mouse_in_keyframe(k) && k.object_id != $program_info[:dragging_keyframe].object_id
          delete_keyframe($program_info[:dragging_keyframe])
          insert_keyframe($program_info[:dragging_keyframe], k)
          break
        end
      end
      $program_info[:dragging_keyframe] = nil
    end
  end
end
