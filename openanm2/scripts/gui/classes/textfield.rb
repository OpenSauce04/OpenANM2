# frozen_string_literal: true

module GuiClassTextField
  class TextField < GuiObject
    attr_reader :group, :name, :function, :attached_vscrollbar, :clip, :anchor_x, :anchor_y
    attr_accessor :text

    def initialize(attr)
      @group = attr[:group]
      @name = attr[:name]
      @text = attr[:text]
      @x = attr[:x]
      @y = attr[:y]
      @w = attr[:w]
      @h = 20
      @function = attr[:function]
      @anchor_x = attr[:anchor_x]
      @anchor_y = attr[:anchor_y]
      @attached_vscrollbar = attr[:attached_vscrollbar]
      @clip = attr[:clip]
    end
  end

  as_trait do
    def add_textfield(attr)
      $gui_objs[:textfields].append TextField.new(attr)
    end

    def mouse_in_textfield(t)
      scrolled_y = t.y - get_vscrollbar_position(t.attached_vscrollbar)
      in_clip_box = if t.clip.nil?
                      true
                    else
                      mouse_in_box(
                        t.clip[:x] + anchor_position(t.anchor_x),
                        t.clip[:y] + anchor_position(t.anchor_y),
                        t.clip[:w],
                        t.clip[:h]
                      )
                    end

      return mouse_in_box(t.x, scrolled_y, t.w, t.h) && in_clip_box
    end

    def render_textfield
      $gui_objs[:textfields].each do |t|
        unless t.clip.nil?
          clip(
            t.clip[:x] + anchor_position(t.anchor_x),
            t.clip[:y] + anchor_position(t.anchor_y),
            t.clip[:w],
            t.clip[:h]
          )
        end
        scrolled_y = t.y - get_vscrollbar_position(t.attached_vscrollbar)
        $program_info[:cursor] = $images[:text_cursor] if mouse_in_textfield(t)
        colour = if mouse_in_textfield(t) && mouse_pressed? && mouse_button == LEFT
                   $theme[:background]
                 elsif $program_info[:focused_textfield] == t.name
                   $theme[:foreground_dark]
                 else
                   $theme[:foreground]
                 end
        cursor = if millis % 1200 >= 600 && $program_info[:focused_textfield] == t.name
                   '|'
                 else
                   ''
                 end
        draw_rect t.x, scrolled_y, t.w, t.h, colour, 5
        draw_text t.text+cursor, t.x + 5, scrolled_y + t.h/2 - 2, LEFT, CENTER
        no_clip
      end
    end

    def click_textfields
      ft = ''
      nft = '' # This means "Next Focused Textfield" lmao

      $gui_objs[:textfields].each do |t|
        ft = t if t.name == $program_info[:focused_textfield]
        nft = t.name if mouse_in_textfield(t) && $program_info[:last_mouse_button] == LEFT
      end

      ft.function.call(ft.text) if ft != ''

      $program_info[:focused_textfield] = if nft != ''
                                            nft
                                          else
                                            nil
                                          end
    end
  end
end
