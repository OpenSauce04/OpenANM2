# frozen_string_literal: true

$gui_objs = {
  anmlayers: [],
  boxes: [],
  dividers: [],
  labels: [],
  buttons: [],
  vscrollbars: [],
  hscrollbars: [],
  keyframes: [],
  checkboxes: [],
  textfields: [],
  icons: [],
  layer_draggables: []
}

module Gui
  as_trait do
    def draw_status_bar
      message = ""
      status_lowfps = frame_rate < $anm_data[:framerate]

      draw_image($images[:slow], width-24, 0, 20, 20) if status_lowfps
      draw_line(width-28, 1, width-28, $layout[:menu_bar][:height]-2, $theme[:foreground])

      message += 'Exporting GIF...' if $program_info[:status][:gif_exporting]
      message += 'GIF exported successfully' if $program_info[:status][:gif_done]
      draw_text(message, width-30, 2, RIGHT)
    end

    def gui_update
      click_vscrollbars
      click_hscrollbars
      click_animation_seeker_bar
    end

    def gui_render
      draw_grid
      render_anmlayers
      render_boxes
      render_dividers
      draw_animation_end
      draw_animation_seeker
      render_keyframes
      render_textfield
      render_checkboxes
      render_buttons
      render_labels
      render_icons
      render_hscrollbars
      render_vscrollbars
      draw_status_bar

      $program_info[:cursor] = MOVE if !$program_info[:dragging_keyframe].nil? ||
                                       !$program_info[:dragging_layer].nil?
    end

    def gui_clear_group(object_array, group)
      (object_array.length - 1).downto(0).each do |i|
        object_array.delete_at(i) if object_array[i].group == group
      end
    end

    def show_alert(message)
      JOptionPane.showMessageDialog(nil, message)
    end

    def show_input_dialog(message)
      return JOptionPane.showInputDialog(nil, message)
    end
  end
end
