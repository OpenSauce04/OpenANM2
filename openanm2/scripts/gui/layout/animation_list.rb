# frozen_string_literal: true

module GuiLayoutAnimationList
  as_trait do
    def init_animation_list
      add_box({
        group: :main,
        name: :animation_list_box,
        x: $layout[:animation_list][:x],
        y: $layout[:animation_list][:y],
        w: $layout[:animation_list][:w],
        h: $layout[:animation_list][:h],
        colour: $theme[:background_light],
        anchor_x: $layout[:animation_list][:anchor_x],
        anchor_y: $layout[:animation_list][:anchor_y]
      })

      add_box({
        group: :main,
        name: :animation_list_box,
        x: $layout[:animation_list][:x],
        y: $layout[:animation_list][:y],
        w: $layout[:animation_list][:w],
        h: $layout[:animation_list][:button_bar_height],
        colour: $theme[:foreground_darker],
        anchor_x: $layout[:animation_list][:anchor_x],
        anchor_y: $layout[:animation_list][:anchor_y]
      })

      add_button({
        group: :main,
        text: '+',
        is_symbol: true,
        x: $layout[:animation_list][:x],
        y: $layout[:animation_list][:y],
        w: $layout[:animation_list][:button_bar_height],
        h: $layout[:animation_list][:button_bar_height],
        anchor_x: $layout[:animation_list][:anchor_x],
        anchor_y: $layout[:animation_list][:anchor_y],
        function: -> { puts 'TODO: Animation adding' }
      })
      add_button({
        group: :main,
        text: '–',
        is_symbol: true,
        x: $layout[:animation_list][:x] + $layout[:animation_list][:button_bar_height],
        y: $layout[:animation_list][:y],
        w: $layout[:animation_list][:button_bar_height],
        h: $layout[:animation_list][:button_bar_height],
        anchor_x: $layout[:animation_list][:anchor_x],
        anchor_y: $layout[:animation_list][:anchor_y],
        function: -> do
          if $anm_data[:current_animation_name] == $anm_data[:default_animation_name]
            show_alert("Can't remove default animation '#{$anm_data[:current_animation_name]}'")
            return
          end

          logs("Removing animation '#{$anm_data[:current_animation_name]}'")
          $anm_data[:animations].remove_child($anm_data[:current_animation])
          load_default_animation
        end
      })
      add_button({
        group: :main,
        text: '★',
        is_symbol: true,
        x: $layout[:animation_list][:x] + $layout[:animation_list][:button_bar_height]*2,
        y: $layout[:animation_list][:y],
        w: $layout[:animation_list][:button_bar_height],
        h: $layout[:animation_list][:button_bar_height],
        anchor_x: $layout[:animation_list][:anchor_x],
        anchor_y: $layout[:animation_list][:anchor_y],
        function: -> do
          $anm_data[:xml_root].get_child('Animations').set_string('DefaultAnimation', $anm_data[:current_animation_name])
          $anm_data[:default_animation_name] = $anm_data[:xml_root].get_child('Animations').get_string('DefaultAnimation')
          generate_anm_animation_buttons
        end
      })
    end
  end

  def generate_anm_animation_buttons
    logs('Rebuilding animation list')
    gui_clear_group($gui_objs[:buttons], :animation_list)
    gui_clear_group($gui_objs[:labels], :animation_list)

    bheight = 20
    i = 0
    $anm_data[:animations].get_children('Animation').each do |animation|
      animation_name = animation.get_string('Name')
      colour_override = if animation_name == $anm_data[:current_animation_name]
                          $theme[:accent]
                        else
                          false
                        end
      if animation_name == $anm_data[:default_animation_name]
        add_label({
          group: :animation_list,
          text: '★',
          is_symbol: true,
          x: $layout[:animation_list][:x] + 2,
          y: $layout[:animation_list][:y] + $layout[:animation_list][:button_bar_height] + i*bheight + 2,
          align_x: LEFT,
          align_y: TOP,
          anchor_x: RIGHT,
          anchor_y: BOTTOM,
          attached_vscrollbar: :animation_list_scroll,
          clip: {
            x: $layout[:animation_list][:x],
            y: $layout[:animation_list][:y] + $layout[:animation_list][:button_bar_height],
            w: $layout[:animation_list][:w],
            h: $layout[:animation_list][:h] - $layout[:animation_list][:button_bar_height]
          }
        })
      end

      add_button({
        group: :animation_list,
        text: animation_name,
        x: $layout[:animation_list][:x],
        y: $layout[:animation_list][:y] + $layout[:animation_list][:button_bar_height] + i*bheight,
        w: $layout[:animation_list][:w] - $layout[:scrollbar][:thickness],
        h: bheight,
        function: -> { load_animation(animation_name) },
        anchor_x: RIGHT,
        anchor_y: BOTTOM,
        colour_override:,
        attached_vscrollbar: :animation_list_scroll,
        clip: {
          x: $layout[:animation_list][:x],
          y: $layout[:animation_list][:y] + $layout[:animation_list][:button_bar_height],
          w: $layout[:animation_list][:w],
          h: $layout[:animation_list][:h] - $layout[:animation_list][:button_bar_height]
        }
      })
      i += 1
    end

    animation_list_box = ''
    $gui_objs[:boxes].each do |b|
      if b.name == :animation_list_box
        animation_list_box = b
        break
      end
    end

    last_scroll_position = 0
    $gui_objs[:vscrollbars].each do |s|
      if s.name == :animation_list_scroll
        last_scroll_position = s.scroll_position/s.scroll_maximum
        break
      end
    end
    gui_clear_group($gui_objs[:vscrollbars], :animation_list)
    scroll_maximum = [i*bheight - animation_list_box.h, 0.01].max
    add_vscrollbar({
      group: :animation_list,
      name: :animation_list_scroll,
      x: -$layout[:scrollbar][:thickness],
      y: $layout[:animation_list][:y] + $layout[:animation_list][:button_bar_height],
      h: $layout[:animation_list][:h] - $layout[:animation_list][:button_bar_height],
      anchor_x: $layout[:animation_list][:anchor_x],
      anchor_y: $layout[:animation_list][:anchor_y],
      scroll_maximum:,
      scroll_position: last_scroll_position*scroll_maximum,
      scroll_magnitude: $layout[:animation_list][:scroll_magnitude],
      scroll_area: $layout[:animation_list][:scroll_area],
    })
  end
end