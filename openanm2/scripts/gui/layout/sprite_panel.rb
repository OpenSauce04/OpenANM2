# frozen_string_literal: true

module GuiLayoutSpritePanel
  as_trait do
    def init_sprite_panel
      add_box({
        group: :main,
        name: :sprite_panel_box,
        x: $layout[:sprite_panel][:x],
        y: $layout[:sprite_panel][:y],
        w: $layout[:sprite_panel][:w],
        h: $layout[:sprite_panel][:h],
        anchor_x: $layout[:sprite_panel][:anchor_x],
        anchor_h: $layout[:sprite_panel][:anchor_h],
        colour: $theme[:foreground_darkest]
      })

      add_label({
        group: :main,
        text: 'Sprite Panel Goes Here',
        x: $layout[:sprite_panel][:x]/2,
        y: $layout[:sprite_panel][:h] + $layout[:sprite_panel][:y] * (3/4),
        anchor_x: $layout[:sprite_panel][:anchor_x],
        anchor_y: BOTTOM,
        align_x: CENTER,
        align_y: CENTER
      })
    end
  end
end