# frozen_string_literal: true

module GuiLayoutAnimationPanel
  as_trait do
    def init_animation_panel
      add_box({
        group: :main,
        name: :animation_panel_keyframe_box,
        x: $layout[:animation_panel][:x],
        y: $layout[:animation_panel][:y] - $layout[:animation_seeker][:h],
        w: $layout[:animation_panel][:w],
        h: $layout[:animation_panel][:h] + $layout[:animation_seeker][:h],
        anchor_y: $layout[:animation_panel][:anchor_y],
        anchor_w: $layout[:animation_panel][:anchor_w],
        colour: $theme[:foreground_darkest]
      })

      add_box({
        group: :main,
        name: :animation_panel_layer_box,
        x: $layout[:animation_panel][:x],
        y: $layout[:animation_panel][:y],
        w: $layout[:animation_panel][:layer_list_width],
        h: $layout[:animation_panel][:h],
        anchor_y: $layout[:animation_panel][:anchor_y],
        colour: $theme[:background_light]
      })
    end

    def generate_animation_panel_content(layerno, layer_id, anm_frames, is_root = false)
      layer = ''
      label = ''
      if is_root
        layer = :root
        label = 'Root'
      else
        $anm_data[:layers].get_children('Layer').each do |_layer|
          if _layer.get_string('Id') == layer_id
            layer = _layer
            label = layer.get_string('Name')
            logs("Loading layer '#{label}'")
            break
          end
        end
      end
      add_label({
        group: :animation_panel,
        text: label,
        x: $layout[:animation_panel][:padding],
        y: $layout[:animation_panel][:y] +
           $layout[:animation_panel][:layer_gap]/2 +
           ($layout[:keyframe][:h] + $layout[:animation_panel][:layer_gap])*(layerno) +
           $layout[:keyframe][:h]/2,
        clip: $layout[:animation_panel][:clip],
        anchor_y: $layout[:animation_panel][:anchor_y],
        align_x: LEFT,
        align_y: CENTER,
        attached_vscrollbar: :animation_panel_vscroll
      })
      unless layer == :root
        add_layer_draggable({
          layeranim: get_layeranim_from_layer(layer),
          x: $layout[:animation_panel][:x],
          y: $layout[:animation_panel][:y] +
             $layout[:animation_panel][:layer_gap]/2 +
             ($layout[:keyframe][:h] + $layout[:animation_panel][:layer_gap])*(layerno),
          w: $layout[:animation_panel][:layer_list_width],
          h: $layout[:keyframe][:h],
          attached_vscrollbar: :animation_panel_vscroll
        })
      end

      divider_y = $layout[:animation_panel][:y] +
                  ($layout[:keyframe][:h] + $layout[:animation_panel][:layer_gap])*(layerno+1)
      add_divider({
        group: :animation_panel,
        x1: $layout[:animation_panel][:layer_list_width],
        y1: divider_y,
        x2: -$layout[:animation_list][:w],
        y2: divider_y,
        clip: $layout[:animation_panel][:clip],
        anchor_y1: BOTTOM,
        anchor_y2: BOTTOM,
        anchor_x2: RIGHT,
        colour: $theme[:background_light],
        attached_vscrollbar: :animation_panel_vscroll
      })
      add_divider({
        group: :animation_panel,
        x1: 0,
        y1: divider_y,
        x2: $layout[:animation_panel][:layer_list_width],
        y2: divider_y,
        clip: $layout[:animation_panel][:clip],
        anchor_y1: BOTTOM,
        anchor_y2: BOTTOM,
        colour: $theme[:foreground_darkest],
        attached_vscrollbar: :animation_panel_vscroll
      })

      x_offset = 0
      frame_position = 0
      frame_id = 0
      anm_frames.each do |frame|
        add_keyframe({
          frame_id:,
          layerno:,
          frame:,
          is_root:,
          x: x_offset,
          frame_position:,
          attached_hscrollbar: :animation_panel_hscroll,
          attached_vscrollbar: :animation_panel_vscroll,
          id: "#{layer_id}.#{frame_id}"
        })
        x_offset += $layout[:keyframe][:w] * frame.get_string('Delay').to_i
        frame_position += frame.get_string('Delay').to_i
        frame_id += 1
      end
    end

    def generate_animation_panel_scroll
      last_vscroll_position = 0
      last_hscroll_position = 0

      $gui_objs[:vscrollbars].each do |s|
        if s.name == :animation_panel_vscroll
          last_vscroll_position = s.scroll_position/s.scroll_maximum
          break
        end
      end
      $gui_objs[:hscrollbars].each do |s|
        if s.name == :animation_panel_hscroll
          last_hscroll_position = s.scroll_position/s.scroll_maximum
          break
        end
      end

      gui_clear_group($gui_objs[:vscrollbars], :animation_panel)
      gui_clear_group($gui_objs[:hscrollbars], :animation_panel)

      vscroll_maximum =
        [
          ($layout[:keyframe][:h] +
            $layout[:animation_panel][:layer_gap]) * ($anm_data[:layers].get_children('Layer').length+1) - # 1 is added to account for the root layer
            $layout[:animation_panel][:h] +
            $layout[:scrollbar][:thickness],
          0.01
        ].max
      hscroll_maximum =
        [
          ($layout[:keyframe][:w] +
            $layout[:animation_panel][:keyframe_gap]) * ($anm_data[:duration]+1) -
            ($layout[:animation_panel][:w] + anchor_position($layout[:animation_panel][:anchor_w])) +
            $layout[:animation_panel][:layer_list_width],
          0.01
        ].max

      add_vscrollbar({
        group: :animation_panel,
        name: :animation_panel_vscroll,
        x: $layout[:animation_panel][:w]-$layout[:scrollbar][:thickness],
        y: $layout[:animation_panel][:y] - $layout[:animation_seeker][:h],
        h: $layout[:animation_panel][:h] + $layout[:animation_seeker][:h],
        anchor_x: $layout[:animation_panel][:anchor_w],
        anchor_y: $layout[:animation_panel][:anchor_y],
        scroll_maximum: vscroll_maximum,
        scroll_position: last_vscroll_position*vscroll_maximum,
        scroll_magnitude: $layout[:animation_panel][:vscroll_magnitude],
        scroll_area: $layout[:animation_panel][:scroll_area]
      })

      add_hscrollbar({
        group: :animation_panel,
        name: :animation_panel_hscroll,
        x: $layout[:animation_panel][:x] + $layout[:animation_panel][:layer_list_width],
        y: $layout[:animation_panel][:y] +
             $layout[:animation_panel][:h] -
             $layout[:scrollbar][:thickness],
        w: $layout[:animation_panel][:w] -
             $layout[:animation_panel][:layer_list_width] -
             $layout[:scrollbar][:thickness],
        anchor_x: $layout[:animation_panel][:anchor_x],
        anchor_y: $layout[:animation_panel][:anchor_y],
        anchor_w: $layout[:animation_panel][:anchor_w],
        scroll_maximum: hscroll_maximum,
        scroll_position: last_hscroll_position*hscroll_maximum,
        scroll_magnitude: $layout[:animation_panel][:hscroll_magnitude]
      })
    end
  end
end