# frozen_string_literal: true

module GuiLayoutFrameEditor
  as_trait do
    def init_frame_editor
      add_box({
        group: :main,
        name: :frame_editor_box,
        x: $layout[:frame_editor][:x],
        y: $layout[:frame_editor][:y],
        w: $layout[:frame_editor][:w],
        h: $layout[:frame_editor][:h],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        colour: $theme[:foreground_darker]
      })

      add_box({
        group: :main,
        name: :animation_property_editor_box,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:w]/2,
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:h]*(5.0/6.0),
        w: $layout[:frame_editor][:w] / 2,
        h: $layout[:frame_editor][:h]/6,
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        colour: $theme[:foreground_darkest]
      })

      add_label({
        group: :main,
        text: 'X Position',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][0],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][0],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Y Position',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][1],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][0],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'X Scale',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][0],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][1],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Y Scale',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][1],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][1],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'X Crop',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][0],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][2],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Y Crop',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][1],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][2],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Width',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][0],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][3],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Height',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][1],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][3],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'X Pivot',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][0],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][4],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Y Pivot',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][1],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][4],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Rotation',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][4],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][5],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      #?----------------------------------------------------------------------------
      add_divider({
        group: :main,
        x1: $layout[:frame_editor][:x] + $layout[:frame_editor][:w]/2,
        y1: $layout[:frame_editor][:y],
        x2: $layout[:frame_editor][:x] + $layout[:frame_editor][:w]/2,
        y2: 0,
        anchor_x1: $layout[:frame_editor][:anchor_x],
        anchor_x2: $layout[:frame_editor][:anchor_x],
        anchor_y1: $layout[:frame_editor][:anchor_y],
        anchor_y2: $layout[:frame_editor][:anchor_y],
        colour: $theme[:foreground]
      })

      add_label({
        group: :main,
        text: 'Red Tint',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][2],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][0],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Red Offset',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][3],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][0],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Green Tint',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][2],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][1],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Green Offset',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][3],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][1],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Blue Tint',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][2],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][2],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Blue Offset',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][3],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][2],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Length',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][2],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][6],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Opacity',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][2],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][3],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Delay',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][2],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][4],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Framerate',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][3],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][6],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Visible?',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][3],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][3],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })

      add_label({
        group: :main,
        text: 'Interpolated?',
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][3],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][4],
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y],
        align_y: TOP,
        align_x: CENTER
      })
    end

    def generate_frame_editor_content(frame, is_root = false)
      logs('Rebuilding frame editor content')
      gui_clear_group($gui_objs[:textfields], :frame_editor)
      gui_clear_group($gui_objs[:checkboxes], :frame_editor)

      add_textfield({
        group: :frame_editor,
        text: frame.get_string('XPosition'),
        name: :XPosition,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][0] - $layout[:frame_editor][:textfield_width]/2,
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][0] + $layout[:frame_editor][:input_offset].y,
        w: $layout[:frame_editor][:textfield_width],
        function: ->(data) { modify_frame(frame, 'XPosition', data, is_root) },
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y]
      })

      add_textfield({
        group: :frame_editor,
        text: frame.get_string('YPosition'),
        name: :YPosition,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][1] - $layout[:frame_editor][:textfield_width]/2,
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][0] + $layout[:frame_editor][:input_offset].y,
        w: $layout[:frame_editor][:textfield_width],
        function: ->(data) { modify_frame(frame, 'YPosition', data, is_root) },
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y]
      })

      add_textfield({
        group: :frame_editor,
        text: frame.get_string('XScale'),
        name: :XScale,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][0] - $layout[:frame_editor][:textfield_width]/2,
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][1] + $layout[:frame_editor][:input_offset].y,
        w: $layout[:frame_editor][:textfield_width],
        function: ->(data) { modify_frame(frame, 'XScale', data, is_root) },
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y]
      })

      add_textfield({
        group: :frame_editor,
        text: frame.get_string('YScale'),
        name: :YScale,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][1] - $layout[:frame_editor][:textfield_width]/2,
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][1] + $layout[:frame_editor][:input_offset].y,
        w: $layout[:frame_editor][:textfield_width],
        function: ->(data) { modify_frame(frame, 'YScale', data, is_root) },
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y]
      })
      unless is_root
        add_textfield({
          group: :frame_editor,
          text: frame.get_string('XCrop'),
          name: :XCrop,
          x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][0] - $layout[:frame_editor][:textfield_width]/2,
          y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][2] + $layout[:frame_editor][:input_offset].y,
          w: $layout[:frame_editor][:textfield_width],
          function: ->(data) { modify_frame(frame, 'XCrop', data, is_root) },
          anchor_x: $layout[:frame_editor][:anchor_x],
          anchor_y: $layout[:frame_editor][:anchor_y]
        })

        add_textfield({
          group: :frame_editor,
          text: frame.get_string('YCrop'),
          name: :YCrop,
          x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][1] - $layout[:frame_editor][:textfield_width]/2,
          y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][2] + $layout[:frame_editor][:input_offset].y,
          w: $layout[:frame_editor][:textfield_width],
          function: ->(data) { modify_frame(frame, 'YCrop', data, is_root) },
          anchor_x: $layout[:frame_editor][:anchor_x],
          anchor_y: $layout[:frame_editor][:anchor_y]
        })

        add_textfield({
          group: :frame_editor,
          text: frame.get_string('Width'),
          name: :Width,
          x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][0] - $layout[:frame_editor][:textfield_width]/2,
          y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][3] + $layout[:frame_editor][:input_offset].y,
          w: $layout[:frame_editor][:textfield_width],
          function: ->(data) { modify_frame(frame, 'Width', data, is_root) },
          anchor_x: $layout[:frame_editor][:anchor_x],
          anchor_y: $layout[:frame_editor][:anchor_y]
        })

        add_textfield({
          group: :frame_editor,
          text: frame.get_string('Height'),
          name: :Height,
          x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][1] - $layout[:frame_editor][:textfield_width]/2,
          y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][3] + $layout[:frame_editor][:input_offset].y,
          w: $layout[:frame_editor][:textfield_width],
          function: ->(data) { modify_frame(frame, 'Height', data, is_root) },
          anchor_x: $layout[:frame_editor][:anchor_x],
          anchor_y: $layout[:frame_editor][:anchor_y]
        })

        add_textfield({
          group: :frame_editor,
          text: frame.get_string('XPivot'),
          name: :XPivot,
          x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][0] - $layout[:frame_editor][:textfield_width]/2,
          y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][4] + $layout[:frame_editor][:input_offset].y,
          w: $layout[:frame_editor][:textfield_width],
          function: ->(data) { modify_frame(frame, 'XPivot', data, is_root) },
          anchor_x: $layout[:frame_editor][:anchor_x],
          anchor_y: $layout[:frame_editor][:anchor_y]
        })

        add_textfield({
          group: :frame_editor,
          text: frame.get_string('YPivot'),
          name: :YPivot,
          x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][1] - $layout[:frame_editor][:textfield_width]/2,
          y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][4] + $layout[:frame_editor][:input_offset].y,
          w: $layout[:frame_editor][:textfield_width],
          function: ->(data) { modify_frame(frame, 'YPivot', data, is_root) },
          anchor_x: $layout[:frame_editor][:anchor_x],
          anchor_y: $layout[:frame_editor][:anchor_y]
        })
      end

      add_textfield({
        group: :frame_editor,
        text: frame.get_string('Rotation'),
        name: :Rotation,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][4] - $layout[:frame_editor][:textfield_width]/2,
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][5] + $layout[:frame_editor][:input_offset].y,
        w: $layout[:frame_editor][:textfield_width],
        function: ->(data) { modify_frame(frame, 'Rotation', data, is_root) },
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y]
      })

      add_textfield({
        group: :frame_editor,
        text: frame.get_string('RedTint'),
        name: :RedTint,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][2] - $layout[:frame_editor][:textfield_width]/2,
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][0] + $layout[:frame_editor][:input_offset].y,
        w: $layout[:frame_editor][:textfield_width],
        function: ->(data) { modify_frame(frame, 'RedTint', data, is_root) },
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y]
      })

      add_textfield({
        group: :frame_editor,
        text: frame.get_string('RedOffset'),
        name: :RedOffset,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][3] - $layout[:frame_editor][:textfield_width]/2,
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][0] + $layout[:frame_editor][:input_offset].y,
        w: $layout[:frame_editor][:textfield_width],
        function: ->(data) { modify_frame(frame, 'RedOffset', data, is_root) },
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y]
      })

      add_textfield({
        group: :frame_editor,
        text: frame.get_string('GreenTint'),
        name: :GreenTint,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][2] - $layout[:frame_editor][:textfield_width]/2,
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][1] + $layout[:frame_editor][:input_offset].y,
        w: $layout[:frame_editor][:textfield_width],
        function: ->(data) { modify_frame(frame, 'GreenTint', data, is_root) },
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y]
      })

      add_textfield({
        group: :frame_editor,
        text: frame.get_string('GreenOffset'),
        name: :GreenOffset,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][3] - $layout[:frame_editor][:textfield_width]/2,
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][1] + $layout[:frame_editor][:input_offset].y,
        w: $layout[:frame_editor][:textfield_width],
        function: ->(data) { modify_frame(frame, 'GreenOffset', data, is_root) },
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y]
      })

      add_textfield({
        group: :frame_editor,
        text: frame.get_string('BlueTint'),
        name: :BlueTint,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][2] - $layout[:frame_editor][:textfield_width]/2,
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][2] + $layout[:frame_editor][:input_offset].y,
        w: $layout[:frame_editor][:textfield_width],
        function: ->(data) { modify_frame(frame, 'BlueTint', data, is_root) },
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y]
      })

      add_textfield({
        group: :frame_editor,
        text: frame.get_string('BlueOffset'),
        name: :BlueOffset,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][3] - $layout[:frame_editor][:textfield_width]/2,
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][2] + $layout[:frame_editor][:input_offset].y,
        w: $layout[:frame_editor][:textfield_width],
        function: ->(data) { modify_frame(frame, 'BlueOffset', data, is_root) },
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y]
      })

      add_textfield({
        group: :frame_editor,
        text: $anm_data[:duration].to_s,
        name: :Length,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][2] - $layout[:frame_editor][:textfield_width]/2,
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][6] + $layout[:frame_editor][:input_offset].y,
        w: $layout[:frame_editor][:textfield_width],
        function: ->(data) do
          $anm_data[:current_animation].set_string('FrameNum', data)
          load_animation($anm_data[:current_animation_name])
        end,
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y]
      })

      add_textfield({
        group: :frame_editor,
        text: frame.get_string('AlphaTint'),
        name: :AlphaTint,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][2] - $layout[:frame_editor][:textfield_width]/2,
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][3] + $layout[:frame_editor][:input_offset].y,
        w: $layout[:frame_editor][:textfield_width],
        function: ->(data) { modify_frame(frame, 'AlphaTint', data, is_root) },
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y]
      })

      add_textfield({
        group: :frame_editor,
        text: frame.get_string('Delay'),
        name: :Delay,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][2] - $layout[:frame_editor][:textfield_width]/2,
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][4] + $layout[:frame_editor][:input_offset].y,
        w: $layout[:frame_editor][:textfield_width],
        function: ->(data) { modify_frame(frame, 'Delay', data, is_root) },
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y]
      })

      add_textfield({
        group: :frame_editor,
        text: $anm_data[:framerate].to_s,
        name: :Fps,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:column][3] - $layout[:frame_editor][:textfield_width]/2,
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:row][6] + $layout[:frame_editor][:input_offset].y,
        w: $layout[:frame_editor][:textfield_width],
        function: lambda do |data|
          $anm_data[:xml_root].get_child('Info').set_string('Fps', data)
          $anm_data[:framerate] = data.to_i
        end,
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y]
      })

      add_checkbox({
        group: :frame_editor,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:input_offset].x + $layout[:frame_editor][:column][3],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:input_offset].y + $layout[:frame_editor][:row][3],
        function: -> { modify_frame(frame, 'Visible', bool_to_string(!frame.get_string('Visible').to_bool), is_root) },
        state: frame.get_string('Visible').to_bool,
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y]
      })

      add_checkbox({
        group: :frame_editor,
        x: $layout[:frame_editor][:x] + $layout[:frame_editor][:input_offset].x + $layout[:frame_editor][:column][3],
        y: $layout[:frame_editor][:y] + $layout[:frame_editor][:input_offset].y + $layout[:frame_editor][:row][4],
        function: -> { modify_frame(frame, 'Interpolated', bool_to_string(!frame.get_string('Interpolated').to_bool), is_root) },
        state: frame.get_string('Interpolated').to_bool,
        anchor_x: $layout[:frame_editor][:anchor_x],
        anchor_y: $layout[:frame_editor][:anchor_y]
      })
    end
  end
end