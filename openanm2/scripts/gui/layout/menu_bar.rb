# frozen_string_literal: true

module GuiLayoutMenuBar
  as_trait do
    def init_menu_bar
      add_box({
        group: :main,
        x: 0,
        y: 0,
        w: 0,
        h: $layout[:menu_bar][:height],
        anchor_w: RIGHT,
        colour: $theme[:foreground_dark]
      })

      add_button({
        group: :main,
        text: 'Open',
        x: 0,
        y: 0,
        w: 50,
        h: $layout[:menu_bar][:height],
        function: lambda {
          file = file_dialog('Open .anm2 file', FILE_DIALOG_LOAD, 'anm2')
          load_anm2_file(file) unless file.nil?
        }
      })

      add_button({
        group: :main,
        text: 'Save',
        x: 50,
        y: 0,
        w: 50,
        h: $layout[:menu_bar][:height],
        function: lambda {
          file = file_dialog('Where to save?', FILE_DIALOG_SAVE, 'anm2')
          save_anm2_file(file) unless file.nil?
        }
      })

      add_button({
        group: :main,
        text: 'Center View',
        x: 100,
        y: 0,
        w: 85,
        h: $layout[:menu_bar][:height],
        function: -> { center_view }
      })

      add_button({
        group: :main,
        text: 'Zoom Out',
        x: 185,
        y: 0,
        w: 75,
        h: $layout[:menu_bar][:height],
        function: -> { zoom_viewer(-3) }
      })

      add_button({
        group: :main,
        text: 'Zoom In',
        x: 260,
        y: 0,
        w: 65,
        h: $layout[:menu_bar][:height],
        function: -> { zoom_viewer(+3) }
      })

      add_button({
        group: :main,
        text: 'Zoom Reset',
        x: 325,
        y: 0,
        w: 85,
        h: $layout[:menu_bar][:height],
        function: -> { $viewer_info[:zoom] = DEFAULT_ZOOM }
      })

      add_button({
        group: :main,
        text: 'Export GIF',
        x: 410,
        y: 0,
        w: 75,
        h: $layout[:menu_bar][:height],
        function: lambda do
          outfile = file_dialog('Where to export?', FILE_DIALOG_SAVE, 'gif')
          return if outfile.nil?
          logs("Exporting gif to #{outfile}")

          $program_info[:status][:gif_exporting] = true
          FileUtils.rm_rf('tmp')

          logs("  Generating gif frames...")
          (0..$anm_data[:duration]).each do |frameno|
            logs("    #{frameno}/#{$anm_data[:duration]}") if frameno % 10 == 0 || frameno == $anm_data[:duration]
            $viewer_info[:animation_timer] = frameno
            pgraphics = create_graphics(1920, 1080)
            pgraphics.no_smooth
            pgraphics.begin_draw
            render_anmlayers(pgraphics)
            pgraphics.end_draw
            pgraphics.save("#{Dir.pwd}/tmp/#{frameno}.frame.png")
          end
          logs("  Finished generating frames")

          logs("  Calling FFMPEG...")
          Thread.new do
            system("#{FFMPEG} -i tmp/%d.frame.png -vf palettegen=reserve_transparent=1 tmp/palette.png -y")
            system("#{FFMPEG} -framerate #{$anm_data[:framerate]} -i tmp/%d.frame.png -i tmp/palette.png -lavfi paletteuse=alpha_threshold=128 -gifflags -offsetting -sws_dither none -f gif \"#{outfile}\" -y")
            system("#{MAGICKCONVERT} -dispose previous \"#{outfile}\" -background \"rgba(0,0,0,0)\" -trim -layers TrimBounds \"#{outfile}\"")
            #FileUtils.rm_rf('tmp')
            $program_info[:status][:gif_exporting] = false
            $program_info[:status][:gif_done] = true
            logs("Finished creating gif")
            sleep 2
            $program_info[:status][:gif_done] = false
          end
        end
      })
    end
  end
end