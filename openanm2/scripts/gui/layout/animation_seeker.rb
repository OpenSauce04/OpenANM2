# frozen_string_literal: true

module GuiLayoutAnimationSeeker
  as_trait do
    def init_animation_seeker
      add_box({
        group: :main,
        name: :animation_seeker_left_filler,
        x: $layout[:animation_seeker][:x] - $layout[:animation_panel][:layer_list_width],
        y: $layout[:animation_seeker][:y],
        w: $layout[:animation_panel][:layer_list_width],
        h: $layout[:animation_seeker][:h],
        anchor_y: $layout[:animation_seeker][:anchor_y],
        colour: $theme[:background_light]
      })

      add_button({
        group: :main,
        text: '⏯',
        is_symbol: true,
        x: $layout[:animation_seeker][:x] - $layout[:animation_seeker][:h]*5,
        y: $layout[:animation_seeker][:y],
        w: $layout[:animation_seeker][:h],
        h: $layout[:animation_seeker][:h],
        anchor_y: $layout[:animation_seeker][:anchor_y],
        function: -> { toggle_animation_paused }
      })

      add_button({
        group: :main,
        text: '▱',
        is_symbol: true,
        x: $layout[:animation_seeker][:x] - $layout[:animation_seeker][:h]*4,
        y: $layout[:animation_seeker][:y],
        w: $layout[:animation_seeker][:h],
        h: $layout[:animation_seeker][:h],
        anchor_y: $layout[:animation_seeker][:anchor_y],
        function: -> do
          delete_layer($program_info[:selected_keyframe].frame.get_parent)
          load_animation($anm_data[:current_animation_name])
        end
      })

      add_button({
        group: :main,
        text: '▰',
        is_symbol: true,
        x: $layout[:animation_seeker][:x] - $layout[:animation_seeker][:h]*3,
        y: $layout[:animation_seeker][:y],
        w: $layout[:animation_seeker][:h],
        h: $layout[:animation_seeker][:h],
        anchor_y: $layout[:animation_seeker][:anchor_y],
        function: -> do
          layer_name = show_input_dialog('Input layer name')
          return if layer_name.nil?
          create_layer(layer_name, $program_info[:selected_keyframe].frame.get_parent)
          load_animation($anm_data[:current_animation_name])
        end
      })

      add_button({
        group: :main,
        text: '–',
        is_symbol: true,
        x: $layout[:animation_seeker][:x] - $layout[:animation_seeker][:h]*2,
        y: $layout[:animation_seeker][:y],
        w: $layout[:animation_seeker][:h],
        h: $layout[:animation_seeker][:h],
        anchor_y: $layout[:animation_seeker][:anchor_y],
        function: -> { delete_keyframe($program_info[:selected_keyframe]) }
      })

      add_button({
        group: :main,
        text: '+',
        is_symbol: true,
        x: $layout[:animation_seeker][:x] - $layout[:animation_seeker][:h],
        y: $layout[:animation_seeker][:y],
        w: $layout[:animation_seeker][:h],
        h: $layout[:animation_seeker][:h],
        anchor_y: $layout[:animation_seeker][:anchor_y],
        function: -> { insert_keyframe($program_info[:selected_keyframe], $program_info[:selected_keyframe], true) }
      })

      add_divider({
        group: :main,
        x1: $layout[:animation_seeker][:x] - $layout[:animation_panel][:layer_list_width] + 2,
        y1: $layout[:animation_seeker][:y] + $layout[:animation_seeker][:h],
        x2: $layout[:animation_panel][:layer_list_width] - 4,
        y2: $layout[:animation_seeker][:y] + $layout[:animation_seeker][:h],
        anchor_y1: BOTTOM,
        anchor_y2: BOTTOM,
        colour: $theme[:foreground_darker]
      })

      add_box({
        group: :main,
        name: :animation_seeker_box,
        x: $layout[:animation_seeker][:x],
        y: $layout[:animation_seeker][:y],
        w: $layout[:animation_seeker][:w],
        h: $layout[:animation_seeker][:h],
        anchor_y: $layout[:animation_seeker][:anchor_y],
        anchor_w: $layout[:animation_seeker][:anchor_w],
        colour: $theme[:foreground_darker]
      })
    end

    def generate_animation_seeker_bar
      gui_clear_group($gui_objs[:dividers], :animation_seeker_bar)
      gui_clear_group($gui_objs[:labels], :animation_seeker_bar)

      (0..$anm_data[:duration]-1).each do |frameno|
        x = $layout[:animation_panel][:padding] +
            $layout[:animation_panel][:layer_list_width] +
            $layout[:keyframe][:w]/2 +
            frameno*$layout[:keyframe][:w]
        pip_height = if frameno%5 == 0 # I don't know what these white lines are called, so I dubbed them pips. It just sounds right
                       6
                     else
                       3
                     end
        add_divider({
          group: :animation_seeker_bar,
          x1: x,
          y1: $layout[:animation_seeker][:y] + $layout[:animation_seeker][:h] - pip_height,
          x2: x,
          y2: $layout[:animation_seeker][:y] + $layout[:animation_seeker][:h],
          anchor_y1: BOTTOM,
          anchor_y2: BOTTOM,
          attached_hscrollbar: :animation_panel_hscroll,
          colour: $theme[:text],
          clip: $layout[:animation_seeker][:clip]
        })

        if frameno%5 == 0
          add_label({
            group: :animation_seeker_bar,
            text: frameno.to_s,
            x:,
            y: $layout[:animation_seeker][:y] + $layout[:animation_seeker][:h] - pip_height + 2,
            anchor_y: BOTTOM,
            attached_hscrollbar: :animation_panel_hscroll,
            align_x: CENTER,
            align_y: BOTTOM,
            colour: $theme[:text],
            clip: $layout[:animation_seeker][:clip]
          })
        end
      end
    end
  end
end