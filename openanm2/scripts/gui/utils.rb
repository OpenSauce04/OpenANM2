# frozen_string_literal: true

module GuiUtils
  as_trait do
    def anchor_position(anchor, divider = 1)
      pos = 0
      pos = width if anchor == RIGHT
      pos = height if anchor == BOTTOM

      return pos / divider
    end

    def center_view
      $viewer_info[:view_offset] =
        Vec2D.new((width-$layout[:sprite_panel][:w])/2, (height-$layout[:animation_panel][:h])/2)
    end
  end
end
