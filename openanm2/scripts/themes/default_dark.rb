# frozen_string_literal: true

module DefaultDarkTheme
  as_trait do
    def load_theme
      return {
        background: color(24, 26, 31),
        background_light: color(33, 37, 43),
        foreground: color(58, 63, 75),
        foreground_dark: color(40, 44, 52),
        foreground_darker: color(26, 30, 36),
        foreground_darkest: color(20, 22, 27),
        accent: color(62, 144, 255),
        root_layer: color(255, 144, 62),
        text: color(250)
      }
    end
  end
end
