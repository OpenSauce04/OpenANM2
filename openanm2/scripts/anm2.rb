# frozen_string_literal: true

$anm_data = {
  framerate: 30,
  duration: 1
}

module Anm2
  as_trait do
    def load_anm2_file(file)
      logs("Loading anm2 file '#{file}'")
      clear_image_cache
      center_view
      $viewer_info[:zoom] = DEFAULT_ZOOM

      sketch_title "OpenANM2 - #{File.basename(file)}"

      $anm_data[:xml_root] = load_xml file
      $anm_data[:directory] = "#{File.dirname(file)}/"
      $anm_data[:framerate] = $anm_data[:xml_root].get_child('Info').get_string('Fps').to_i
      $anm_data[:animations] = $anm_data[:xml_root].get_child('Animations')

      anm_content = $anm_data[:xml_root].get_child('Content')
      $anm_data[:sheets] = anm_content.get_child('Spritesheets')
      $anm_data[:layers] = anm_content.get_child('Layers')

      load_default_animation
    end

    def load_default_animation
      $anm_data[:default_animation_name] = $anm_data[:xml_root].get_child('Animations').get_string('DefaultAnimation')

      default_animation_exists = false
      $anm_data[:animations].get_children('Animation').each do |animation|
        if animation.get_string('Name') == $anm_data[:default_animation_name]
          default_animation_exists = true
          break
        end
      end

      if default_animation_exists
        load_animation($anm_data[:default_animation_name])
      else
        show_alert("Default animation '#{$anm_data[:default_animation_name]}' doesn't exist. Please correct this.")
        load_animation($anm_data[:animations].get_children('Animation')[0].get_string('Name') )
      end
    end

    def load_animation(animation_name)
      logs("Loading animation '#{animation_name}'")
      gui_clear_group($gui_objs[:anmlayers], :current_animation)
      gui_clear_group($gui_objs[:keyframes], :animation_panel)
      gui_clear_group($gui_objs[:layer_draggables], :animation_panel)
      gui_clear_group($gui_objs[:dividers], :animation_panel)
      gui_clear_group($gui_objs[:labels], :animation_panel)
      gui_clear_group($gui_objs[:textfields], :frame_editor)
      gui_clear_group($gui_objs[:checkboxes], :frame_editor)

      if animation_name == $anm_data[:current_animation_name]
        unless $program_info[:selected_keyframe].nil?
          $anm_data[:duration] = $anm_data[:current_animation].get_string('FrameNum').to_i
          generate_frame_editor_content($program_info[:selected_keyframe].frame, $program_info[:selected_keyframe].is_root)
        end
      else
        $viewer_info[:animation_timer_start] = millis
        $program_info[:selected_keyframe] = nil
      end

      layeranims = ''
      $anm_data[:duration] = ''
      $anm_data[:animations].get_children('Animation').each do |animation|
        next unless animation.get_string('Name') == animation_name
        $anm_data[:current_animation] = animation
        $anm_data[:current_animation_name] = animation_name
        $anm_data[:duration] = animation.get_string('FrameNum').to_i
        $anm_data[:root_animation] = animation.get_child('RootAnimation')
        $anm_data[:baked_root_animation] = bake_layeranim($anm_data[:root_animation])
        layeranims = animation.get_child('LayerAnimations').get_children('LayerAnimation')
        break
      end

      layerno = 1

      layeranims.each do |layer|
        baked_layer_frames = bake_layeranim(layer)
        layer_id = layer.get_string('LayerId')
        anm_sprite_path = get_layer_sprite(layer_id)
        anm_sprite_image = cached_load_image(anm_sprite_path) unless anm_sprite_path.nil?
        add_anmlayer({
          group: :current_animation,
          sprite: anm_sprite_image,
          frames: baked_layer_frames
        })
        generate_animation_panel_content(layerno, layer_id, layer.get_children('Frame'))
        layerno += 1
      end
      generate_animation_panel_content(0, -1, $anm_data[:root_animation].get_children('Frame'), true)

      generate_animation_panel_scroll
      generate_anm_animation_buttons
      generate_animation_seeker_bar
    end

    def modify_frame(frame, property, value, is_root)
      frame.set_string(property, value)
      load_animation($anm_data[:current_animation_name])
      generate_frame_editor_content(frame, is_root)
    end

    def get_layer_sprite(layer_id)
      $anm_data[:layers].get_children('Layer').each do |layer|
        next unless layer.get_string('Id') == layer_id

        sheet_id = layer.get_string('SpritesheetId')
        $anm_data[:sheets].get_children('Spritesheet').each do |sheet|
          if sheet.get_string('Id') == sheet_id
            return discover_path_case($anm_data[:directory] + sheet.get_string('Path'))
          end
        end
      end
    end

    def bake_keyframe(frame, next_frame)
      baked_frames = []
      delay = frame.get_string('Delay').to_i

      (1..delay).each do |_|
        bframe = parse_xml(frame.to_string)
        bframe.set_string('Delay', '1')
        baked_frames.append(bframe)
      end

      if delay <= 1 ||
         next_frame == '' ||
         frame.get_string('Interpolated') != 'true'
        return baked_frames
      end

      ANM_INTERPOLATABLES.each do |attr|
        j = 1
        interpolate_numbers(frame.get_string(attr).to_f, next_frame.get_string(attr).to_f, delay - 1).each do |new_value|
          baked_frames[j].set_string(attr, new_value.to_s)
          j += 1
        end
      end

      return baked_frames
    end

    def bake_layeranim(layer)
      cache = $cache[:baked_layers][layer.to_string]
      return cache if not cache.nil?

      frames = layer.get_children('Frame')
      new_frames = []

      (0..frames.length-1).each do |i|
        frame = frames[i]
        next_frame = ''
        next_frame = frames[i + 1] unless i + 1 == frames.length
        bake_keyframe(frame, next_frame).each do |baked_frame|
          new_frames.append(baked_frame)
        end
      end
      $cache[:baked_layers][layer.to_string] = new_frames
      return new_frames
    end

    def save_anm2_file(path)
      File.open(path, 'w') do |file|
        doc = REXML::Document.new($anm_data[:xml_root].to_string)
        out = +''
        doc.write(out, 1)
        file.write(out)
      end
    end

    def purge_layer(layer)
      removed_id = layer.get_string('LayerId').to_i

      $anm_data[:layers].get_children('Layer').each do |l|
        next if l.get_string('Id').to_i != removed_id
        $anm_data[:layers].remove_child(l)
        break
      end

      remove_layeranim(layer)
    end

    def remove_layeranim(layer)
      $anm_data[:current_animation].get_child('LayerAnimations').remove_child(layer)
    end

    # Inserts `layer` above `destination_layer`
    def insert_layeranim(layer, destination_layer)
      layeranims = $anm_data[:current_animation].get_child('LayerAnimations')
      tmp_layers = []
      layeranims.get_children('LayerAnimation').reverse_each do |la|
        if la.to_string == destination_layer.to_string
          layeranims.add_child(layer)
          tmp_layers.each do |la|
            layeranims.add_child(la)
          end
          break
        end
        tmp_layers.append(la)
        layeranims.remove_child(la)
      end
    end

    def swap_layeranims(layer_a, layer_b)
      layeranims = $anm_data[:current_animation].get_child('LayerAnimations')
      tmp_layers = []

      layeranims.get_children('LayerAnimation').each do |la|
        if la.to_string == layer_a.to_string
          tmp_layers.append(layer_b)
        elsif la.to_string == layer_b.to_string
          tmp_layers.append(layer_a)
        else
          tmp_layers.append(la)
        end
        layeranims.remove_child(la)
      end

      tmp_layers.each do |la|
        layeranims.add_child(la)
      end
    end

    def create_layer(layer_name, destination_layer)

      next_layer_id = -1
      $anm_data[:layers].get_children('Layer').each do |l|
        next_layer_id = [l.get_string('Id').to_i, next_layer_id].max
      end
      next_layer_id += 1

      $anm_data[:layers].add_child(parse_xml("<Layer Name=\"#{layer_name}\" Id=\"#{next_layer_id}\" SpritesheetId=\"0\"/>"))

      insert_layeranim(
        parse_xml("<LayerAnimation LayerId=\"#{next_layer_id}\" Visible=\"true\"/>"),
        destination_layer
        )
    end

    def get_layeranim_from_layer(layer)
      return nil if layer == ''
      $anm_data[:current_animation].get_child('LayerAnimations').get_children('LayerAnimation').each do |la|
        return la if la.get_string('LayerId') == layer.get_string('Id')
      end
    end
  end
end

ANM_INTERPOLATABLES = %w[
  XPosition
  YPosition
  XScale
  YScale
  Rotation
  RedTint
  GreenTint
  BlueTint
  AlphaTint
  RedOffset
  GreenOffset
  BlueOffset
].freeze
