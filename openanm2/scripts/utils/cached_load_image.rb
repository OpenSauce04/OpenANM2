# frozen_string_literal: true

module CachedLoadImage
  as_trait do
    def cached_load_image(path)
      $cache[:images].each do |i|
        return i[:image] if i[:path] == path
      end

      image = load_image(path)
      $cache[:images] << {
        image:,
        path:
      }

      return image
    end

    def clear_image_cache
      $cache[:images] = []
    end
  end
end
