# frozen_string_literal: true

module FileDialogHelper
  as_trait do
    $selected_file = false

    def file_dialog(title, mode, ext_filter = '')
      logs("Creating file dialog (mode #{mode})")
      fd = FileDialog.new(nil, title, mode)
      fd.setFile "*.#{ext_filter}"
      fd.setFilenameFilter(->(_, name) { name.end_with?(".#{ext_filter}") })
      fd.setVisible true # Execution stops here until file selection is completed
      return fd.getDirectory + fd.getFile unless fd.getFile.nil?
    end
  end
end
