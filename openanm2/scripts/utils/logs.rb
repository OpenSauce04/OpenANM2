# frozen_string_literal: true

def logs(string)
  puts "[#{Time.now.strftime("%H:%M:%S")}] #{string}"
end