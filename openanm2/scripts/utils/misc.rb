# frozen_string_literal: true

module Utils
  as_trait do
    def mouse_in_box(x, y, w, h)
      return mouse_x > x &&
             mouse_y > y &&
             mouse_x <= x+w &&
             mouse_y <= y+h
    end

    def box_in_box(x1, y1, w1, h1, x2, y2, w2, h2)
      return x1 + w1 > x2 &&
             y1 + h1 > y2 &&
             x1 < x2 + w2 &&
             y1 < y2 + h2
    end

    def mouse_pressed_in_box(x, y, w, h)
      return mouse_in_box(x, y, w, h) &&
             mouse_pressed? &&
             mouse_button == LEFT
    end

    def mouse_is_captured
      return true if
        mouse_x >= width ||
        mouse_x <= 0 ||
        mouse_y >= height ||
        mouse_y <= 0

      ($gui_objs[:boxes] + $gui_objs[:vscrollbars] + $gui_objs[:buttons]).flatten.each do |o|
        return true if mouse_in_box(o.x, o.y, o.w, o.h)
      end
      return false
    end

    def ci_glob(path)
      glob = ''
      drive_letter_skipped = false
      path.each_char do |c|
        if Gem.win_platform? and !drive_letter_skipped
          drive_letter_skipped = true
          glob += c
          next
        end
        glob += c.downcase == c.upcase ? c : "[#{c.downcase}#{c.upcase}]"
      end
      return glob
    end

    def discover_path_case(path)
      return Dir.glob(ci_glob(path.tr('\\', '/'))).first
    rescue
      return nil
    end

    def interpolate_numbers(a, b, steps)
      interpolated_array = []
      (1..steps).each do |i|
        interpolated_array.append (b - a) * i / (steps + 1) + a
      end
      return interpolated_array
    end

    def bool_to_string(bool)
      return 'True' if bool == true
      return 'False'
    end
  end
end
