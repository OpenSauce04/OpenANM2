# frozen_string_literal: true

$shaders = {}

module ShadersLoad
  as_trait do
    def load_shaders
      $shaders[:colour_offset] = load_shader('shaders/colour_offset.frag', 'shaders/colour_offset.vert')
    end
  end
end
