# frozen_string_literal: true

module EventMousePressed
  as_trait do
    def mouse_pressed
      begin_drag_keyframes
      begin_drag_layers
    end
  end
end
