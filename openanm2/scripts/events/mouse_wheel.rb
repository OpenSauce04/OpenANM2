# frozen_string_literal: true

module EventMouseWheel
  as_trait do #* Fix name case when this issue gets fixed: https://github.com/ruby-processing/propane/issues/44
    def mouseWheel(event) # rubocop:disable Naming/MethodName
      amount = event.get_count
      scroll_vscrollbars(amount)
      scroll_hscrollbars(amount)
      zoom_viewer(-amount) if !mouse_is_captured
    end
  end
end