# frozen_string_literal: true

module EventKeyPressed
  as_trait do
    def key_pressed
      if key == "\n"
        $gui_objs[:textfields].each do |t|
          if t.name == $program_info[:focused_textfield]
            t.function.call(t.text)
            $program_info[:focused_textfield] = nil
          end
        end
      end

      unless $program_info[:focused_textfield].nil?
        $gui_objs[:textfields].each do |t|
          if t.name == $program_info[:focused_textfield]
            if key == "\b"
              t.text = t.text[0...-1]
            elsif key.is_a?(String) && key.match(/^[[:print:]]+$/)
              t.text << key
            end
            return
          end
        end
      end

      case key
      when "\b"
        delete_keyframe($program_info[:selected_keyframe])
      when ' '
        toggle_animation_paused
      end
    end
  end
end
