# frozen_string_literal: true

module EventMouseReleased
  as_trait do
    def mouse_released
      click_textfields
      click_checkboxes
      click_buttons
      drag_keyframes
      drag_layers
      click_keyframes
    end
  end
end
