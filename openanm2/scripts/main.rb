# frozen_string_literal: true

require './scripts/utils/logs'

logs('Showing splash')
require './splash'

logs('Loading gems')
require 'fileutils'
require 'modularity'
require 'pathname'
require 'propane'
require 'rexml/document'
require 'to_bool'

logs('Loading Java components')
java_import 'java.awt.Toolkit'
java_import 'java.awt.FileDialog'
java_import 'javax.swing.JOptionPane'

logs('Loading scripts')
# Priority scripts
require './scripts/gui/classes/gui_object'
# Load everything else
Dir.glob('scripts/**/*.rb').each do |file|
  require "./#{file}" unless file == 'main.rb'
end

class OpenANM2 < Propane::App
  load_library :file_chooser

  logs('Performing mixin')
  # Misc Important Stuff
  include Utils
  include FileDialogHelper
  include CachedLoadImage
  include ShadersLoad
  # Main ANM2 code
  include Anm2
  # Theme
  include DefaultDarkTheme
  # GUI Utilities
  include GuiUtils
  # GUI Drawables
  include GuiDrawableRect
  include GuiDrawableQuad
  include GuiDrawableTriangle
  include GuiDrawableCircle
  include GuiDrawableText
  include GuiDrawableLine
  include GuiDrawableGrid
  include GuiDrawableImage
  include GuiDrawableAnimationSeeker
  include GuiDrawableAnimationEnd
  # GUI Classes
  include GuiClassBox
  include GuiClassDivider
  include GuiClassLabel
  include GuiClassButton
  include GuiClassAnmLayer
  include GuiClassVScrollbar
  include GuiClassHScrollbar
  include GuiClassKeyframe
  include GuiClassCheckbox
  include GuiClassTextField
  include GuiClassIcon
  include GuiClassLayerDraggable
  include AnimationSeekerBar
  # GUI Layouts
  include GuiLayoutAnimationList
  include GuiLayoutAnimationPanel
  include GuiLayoutAnimationSeeker
  include GuiLayoutFrameEditor
  include GuiLayoutMenuBar
  include GuiLayoutSpritePanel
  # Main GUI code
  include Gui
  include GuiLayoutInit
  include ViewerFunctions
  # Events
  include EventKeyPressed
  include EventMousePressed
  include EventMouseReleased
  include EventMouseWheel

  $viewer_info = {}
  $program_info = {
    status: {}
  }
  $cache = {
    images: [],
    baked_layers: {}
  }

  def settings
    logs('Setting window properties')
    screen_size = Toolkit.getDefaultToolkit.getScreenSize
    size screen_size.getWidth, screen_size.getHeight, P2D
    PJOGL.setIcon('resources/images/icon.png')
  end

  def setup
    logs('Running setup procedure')
    sketch_title 'OpenANM2'
    frame_rate PROGRAM_FRAMERATE
    native_surface = surface.get_native
    native_surface.set_resizable true
    native_surface.set_maximized true, true
    g.texture_sampling 2 # Disable smoothing

    load_shaders
    $screen_size = Vec2D.new(width, height)
    $viewer_info[:animation_timer] = 0
    $viewer_info[:animation_timer_start] = 0
    center_view
    $viewer_info[:zoom] = DEFAULT_ZOOM
    $images = {
      slow: load_image('resources/images/slow.png'),
      # Below is done to circumvent the active spot of the `TEXT` cursor type being completely wrong for whatever reason
      text_cursor: load_image("#{Gem.loaded_specs['propane'].gem_dir}/src/main/resources/cursors/text.png")
    }
    $fonts = {
      regular: create_font('resources/fonts/heebo.ttf', 13, true),
      symbol: create_font('resources/fonts/symbola.ttf', 17, true)
    }
    text_font($fonts[:regular])
    $theme = load_theme
    gui_layout_init

    logs('Killing splash')
    $splash_window.dispose
  end

  $program_info[:last_mouse_button] = 0

  def draw
    $screen_size = Vec2D.new(width, height)
    unless $viewer_info[:paused]
      $viewer_info[:animation_timer] = ((millis - $viewer_info[:animation_timer_start]) / (1000/$anm_data[:framerate])).to_i % $anm_data[:duration]
    end
    background $theme[:background]
    $program_info[:cursor] = ARROW

    pan_viewer if !mouse_is_captured && mouse_button == LEFT

    gui_update
    gui_render

    cursor($program_info[:cursor])

    $program_info[:last_mouse_button] = mouse_button if mouse_button != 0
  end
end

OpenANM2.new
