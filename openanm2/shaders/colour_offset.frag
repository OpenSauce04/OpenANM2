#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform sampler2D texture;

varying vec4 vertColor;
varying vec4 vertTexCoord;

uniform float ro;
uniform float go;
uniform float bo;

void main() {
  gl_FragColor = texture2D(texture, vertTexCoord.st) * vertColor + vec4(ro, go, bo, 0.);
}