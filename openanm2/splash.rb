# frozen_string_literal: true

java_import 'javax.swing.JFrame'
java_import 'javax.swing.JLabel'
java_import 'javax.swing.ImageIcon'

class MainWindow < JFrame
  def initialize
    super 'OpenANM2'
    setUndecorated true
    setType Type.valueOf("POPUP") # This workaround is necessary; For whatever reason Type.POPUP doesn't exist

    image = JLabel.new(ImageIcon.new('./resources/images/splash.png'))
    image.setVisible true
    add image

    setSize(800, 400)
    setLocationRelativeTo(nil)
  end
end

$splash_window = MainWindow.new
$splash_window.setVisible true
